name             'django_app'
maintainer       'Digital Foundation'
maintainer_email 'dev@digital.foundation'
license          'All rights reserved'
description      'Installs/Configures django_app'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
