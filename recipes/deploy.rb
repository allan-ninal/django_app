#
# Cookbook Name:: django_app
# Recipe:: deploy
#
# Copyright 2015, Digital.Foundation
#
# All rights reserved - Do Not Redistribute
#

service 'apache2' do
   action [ :enable, :start ]
end

remote_directory "/var/www/html" do
   source "django_app"
   files_mode "0644"
   mode "0755"
end

bash 'enable_wsgi' do
   code <<-EOH
a2enmod wsgi
EOH
end

cookbook_file "/etc/apache2/sites-available/000-default.conf" do
   source "000-default.conf"
   mode "0644"
end

bash 'restart_apache' do
   code <<-EOH
service apache2 restart
EOH
end

bash 'import_database' do
code <<-EOH
psql -f /var/www/html/live_backup.dump --host=django-opsworks.cmfyw35ux9ic.ap-southeast-2.rds.amazonaws.com --port=5432 --username=mcwilliamsmaster --password --dbname=mcwilliams_wine_db
777mcwilliams_db_master777
EOH
end
