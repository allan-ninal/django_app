#
# Cookbook Name:: django_app
# Recipe:: default
#
# Copyright 2015, Digital.Foundation
#
# All rights reserved - Do Not Redistribute
#

package 'apache2' do
   action :install
end

package 'python' do
   action :install
end

package 'python-dev' do
   action :install
end

package 'python-tz' do
   action :install
end

package 'python-pip' do
   action :install
end

package 'libapache2-mod-wsgi' do
   action :install
end

package 'libpq-dev' do
   action :install
end

package 'binutils' do
   action :install
end

package 'libproj-dev' do
   action :install
end

package 'gdal-bin' do
   action :install
end

package 'postgresql' do
   action :install
end

package 'postgresql-contrib' do
   action :install
end

bash 'install_python_modules' do
   code <<-EOH
pip install django django-jfu easy_thumbnails  django-storages psycopg2 reportlab -U boto
EOH
end
