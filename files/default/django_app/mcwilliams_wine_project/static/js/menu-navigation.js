// Dynamic Menu Navigation Mechanics

var $parent = $('.aside ul.nav');
var $nav_menus = $parent.find('li[data-nav-id]');
var current_abs_url = document.URL.split('/');

var current_url = current_abs_url.slice(3,99).join('/');

$nav_menus.removeClass('active');
var $li_to_be_selected = $parent.find("li[data-nav-id='" + current_url + "']");

$li_to_be_selected.addClass('active');
