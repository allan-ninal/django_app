function initTooltip(){

    $('.tooltip').remove();

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body',
        placement : 'top'
    });

}

function initTooltipBottom(){

    $('.tooltip').remove();

    $('[data-toggle="tooltip"]').tooltip({
        container: 'body',
        placement : 'bottom'
    });

}


function isElementInViewport(el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var r, html;
    if ( !el || 1 !== el.nodeType ) { return false; }
    html = document.documentElement;
    r = el.getBoundingClientRect();

    return ( !!r
      && r.bottom >= 0
      && r.right >= 0
      && r.top <= html.clientHeight
      && r.left <= html.clientWidth
    );
}


function getVisibleHeight($el) {

    var scrollTop = $(this).scrollTop(),
        scrollBot = scrollTop + $(this).height(),
        elTop = $el.offset().top,
        elBottom = elTop + $el.outerHeight(),
        visibleTop = elTop < scrollTop ? scrollTop : elTop,
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;

   return visibleBottom - visibleTop;
}


function notifyShow(msg, opts, settings){

    var $header = $('header.topnavbar-wrapper');
    var visible_height = getVisibleHeight($header);
    var offset = isElementInViewport($header) ? visible_height : 0;

    if (msg == undefined){
        msg = 'Action successfully completed';
    }

    if (opts == undefined){

        opts = {
            type:'success',
            delay: 2000,
            animate:{
                enter: 'animated bounceInDown',
		        exit: 'animated bounceOutUp'
            },
            placement: {
                align:'center'
            },
            offset: offset
        }

    }

    if (settings == undefined){
        settings = {
            message: msg
        }
    }

    $.notify(
        msg,
        opts,
        settings
    );
}


function openDialog(url, id, options, on_shown) {

    $('#'+id).remove();

    var dialog_obj = bootbox.dialog(options);
    var $container = dialog_obj.prop('id', id);

    var $body = $container.find('.modal-body');
    var loading_class = 'whirl traditional';

    $body.addClass(loading_class);

    $.get(url, function (response) {

        if (response.success) {

            $body.html(response.html_content);
            $body.removeClass(loading_class);

            if (on_shown != undefined) {
                on_shown();
            }

        }

    });
}


function initSection($container, url_from, show_loading){

    if (show_loading == undefined){
        show_loading = true;
    }

    if (show_loading){
        var loading_class = 'whirl traditional';

        $container.css({'minHeight': '20%'});
        $container.addClass(loading_class);
    }


    $.get(url_from, function (response) {

        if (response.success){

            $container.html(response.html_content);

            if (show_loading){
                $container.css('minHeight', '');
                $container.removeClass(loading_class);
            }

        }

    });

}

//    $.get(url, function(response){
//        var $container = $('#dialog_container');
//        var $dialog = $('#'+id);
//        if($dialog.length){
//            try{
//                $dialog.dialog("destroy");
//            }catch(a){
//
//            }
//            $dialog.remove();
//        }
//
//        $container.append('<div id="'+id+'"></div>');
//        $dialog = $container.find('#'+id);
//
//        $dialog.html(response);
//        $container.find('#'+id).dialog(option);
//
//    });
//}


function blockUICss(message){

    if (message == undefined){
        message = '<h3>Please wait a few seconds.<br/> We are saving the slide.</h3>';
    }

    $.blockUI({
        css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
//                zIndex: 1050
        },
        baseZ: 1050,
        message: message
    });
}

function parseURL(str){
    console.log('----', str);
//    var arr = str.split(' ');
//    console.log('----', arr);
//    console.log($.grep(arr, function(n, i){
//        return n.match(/custom/);
//    }));
//    return $.grep(arr, function(n, i){
//        return n.match(/custom/);
//    })[0]
}


function showAssetsTreePopupGeneral(self){

    self = $(self);

    var options = {
        message: 'Getting list of files ...',
        title: "Choose file",
        buttons: {
            Cancel: {
                callback: function() {
                    $(this).modal('hide');
                }
            },
            'Choose': {
                className: "btn-primary",
                callback: function() {

                    var $container = $('.bootbox.modal');
                    var $body = $container.find('.modal-body');
                    var $active_li = $body.find('.easytree-active').filter(function(index){
                        return $(this).prop('class').match(/custom/);
                    });

                    if ($active_li.length == 0){
                        return false;
                    }

                    var arr = $active_li.prop('class').split(' ');
                    var active_li_tree_class = $.grep(arr, function(n, i){
                        return n.match(/custom/);
                    })[0];
                    var active_li_parent_path = $.grep(arr, function(n, i){
                        return n.match(/path/);
                    })[0].split('___')[1];

                    var active_li_tree_data = getAssetData(active_li_tree_class);

                    // if selected brand/range/sku but not the Docs/Imgs -> disable click in this case
                    if (!active_li_tree_data['subtype']){
                        return false;
                    }

                    var filename = self.parents('tr').first().find('p.name').text();
                    var subtype = active_li_tree_data['subtype'];
                    var type = active_li_tree_data['type'];
                    var id = active_li_tree_data['id'];

                    $.get('/asset/will_override/', {'subtype': subtype, 'type': type, 'id': id, 'filename': filename}, function(response){
                        if (response.will_override){

                            bootbox.dialog({
                                message: 'File already exists. Would you like to overwrite it?',
                                title: "Confirmation",
                                buttons: {
                                    No: {
                                        className: "btn-primary",
                                        callback: function() {
                                            $(this).modal('hide');
                                        }
                                    },
                                    Yes: {
                                        className: "btn-danger",
                                        callback: function() {
                                            for (var key in active_li_tree_data){
                                                if (active_li_tree_data.hasOwnProperty(key)){
                                                    self.data(key, active_li_tree_data[key]);
                                                }
                                            }

                                            self
                                                .text('...' + active_li_parent_path.slice(-12))
                                                .removeClass('btn-primary')
                                                .addClass('btn-success')
                                                .attr('data-toggle', "tooltip")
                                                .attr('data-original-title', active_li_parent_path);

                                            initTooltipBottom();
                                            $(this).modal('hide');
                                        }
                                    }
                                }
                            });

                        } else {

                            for (var key in active_li_tree_data){
                                if (active_li_tree_data.hasOwnProperty(key)){
                                    self.data(key, active_li_tree_data[key]);
                                }
                            }

                            self
                                .text('...' + active_li_parent_path.slice(-12))
                                .removeClass('btn-primary')
                                .addClass('btn-success')
                                .attr('data-toggle', "tooltip")
                                .attr('data-original-title', active_li_parent_path);

                            initTooltipBottom();

                        }
                    });

                }
            }
        }
    };
    var url = '/asset/list/tree/';

    openDialog(url, 'some_id_here_2', options, function(){

    });

}


//function parseURL(str){
//    console.log('----', str);
//    var arr = str.split(' ');
//    console.log('----', arr);
////    console.log($.grep(arr, function(n, i){
////        return n.match(/custom/);
////    }));
//    return $.grep(arr, function(n, i){
//        return n.match(/custom/);
//    })[0]
//}


function getAssetData(str){
    var str_arr = str.split('_');

    var data = {
        'prefix': str_arr[0],
        'type': str_arr[1],
        'id': str_arr[2]
    };

    if(str_arr.length > 3){
        data['subtype'] = str_arr[3]
    }

    return data
}


function getStateOptions(){
    var country_id = $('#distributor_countries_select').find('option:selected').val();
    var $states_container = $('#distributor_states_select');

    $states_container.chosen().change(function(e, params){
         getDistributorPriceListForm();
     //values is an array containing all the results.
    });

    $.get('/state/options/?country_id=' + country_id, function (response) {
        if (response.success){
            $states_container.html(response.html_content);
            $('.chosen').trigger('chosen:updated');
        }
    })
}


function getCountriesAndStates(){

    var distributor_id = $('#user-distributor-select').find('option:selected').val();
    var $container = $('#countries-and-states-container');

    $.get('/user/country_and_states_options/?distributor_id=' + distributor_id, function(response){

        if (response.success){
            $container.html(response.html_content);
            $('.chosen').trigger('chosen:updated');
        }

    })
}


function getDistributorPriceListForm(){

    var $container = $('#distributor_files');
    var $state_options = $('#distributor_states_select');

    var country_id = $('#distributor_countries_select').find('option:selected').val();
    var distributor_id = $('#distributor_id').val();
    var data = '';

    data += 'country_id=' + country_id;

    if ($state_options.find('option:selected').length > 0){
        var state_ids = $state_options.val().toString();
        data += '&state_ids=' + state_ids;
    }

    if (distributor_id != undefined){
        data += '&distributor_id=' + distributor_id;
    }

    $container.show();

    $.get('/distributor/pricelist_forms/', data, function(response){

        if (response.success){
            $container.html(response.html_content);

                if($state_options.children().length > 0 && $state_options.find('option:selected').length == 0){
                    $container.hide();
                }
        }

    });

}


function getDistributorsCountriesAndStates(){

    var role_id = $('.user-type-select').find('option:selected').val();
    var $container = $('.distributors-countries-and-states-container');

    // is Content Manager
    if (role_id == '2'){

        $container.hide().find('input, select').each(function(){
            $(this).prop('disabled', true);
        })
    } else {
        $container.show().find('input, select').each(function(){
            $(this).prop('disabled', false);
        })
    }
    $('.chosen').trigger('chosen:updated');
}
