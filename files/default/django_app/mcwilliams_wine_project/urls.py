from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mcwilliams_wine_project.views.home', name='home'),

    url(r'', include('mcwilliams_wine_project.apps.web.urls')),
    url(r'^api/', include('mcwilliams_wine_project.apps.api.urls')),

    url(r'^admin/', include(admin.site.urls)),


    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
)

# if settings.DEBUG:
#     urlpatterns += patterns('',
#         (r'^static/(?P<path>.*)$','django.views.static.serve', {'document_root': settings.STATIC_ROOT})
#     )
