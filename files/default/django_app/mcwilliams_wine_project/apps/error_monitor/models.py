from django.db import models
from django.utils.translation import ugettext_lazy as _


class ProjectException(models.Model):
    path = models.CharField(max_length=255)
    contents = models.TextField()
    title = models.TextField(null=True, blank=True)
    location = models.PositiveIntegerField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _("project exception")
        verbose_name_plural = _("project exceptions")

