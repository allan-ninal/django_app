from .models import ProjectException

from django.views.debug import ExceptionReporter

import sys


class ExceptionMiddleware(object):
    def process_exception(self, request, exception):
        if request is not None:
            current_exception = sys.exc_info()
            t = ExceptionReporter(request, *current_exception)
            html_content = t.get_traceback_html()
            ProjectException.objects.create(
                                            path=request.path,
                                            contents=html_content,
                                            title=str(exception)
            )
