# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectException',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('path', models.CharField(max_length=255)),
                ('contents', models.TextField()),
                ('title', models.TextField(null=True, blank=True)),
                ('location', models.PositiveIntegerField(null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'project exception',
                'verbose_name_plural': 'project exceptions',
            },
            bases=(models.Model,),
        ),
    ]
