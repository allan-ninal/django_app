from .views import view_handled_exception
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(
        r'^view/(?P<exception_id>\d+)/$',
        view_handled_exception,
        name='view_handled_exception'
        ),
)

