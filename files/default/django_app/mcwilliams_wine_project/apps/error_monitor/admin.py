from .models import ProjectException

from django.contrib import admin
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, render_to_response


def exception_view_link(object):
    return '<a target="_blank" href="' + \
        reverse(
                "admin:exception_content_views",
                kwargs={'object_id': str(object.id)}
                ) + '">Details</a>'

exception_view_link.short_description = 'View exception'
exception_view_link.allow_tags = True


class ProjectExceptionAdmin(admin.ModelAdmin):
    list_display = ('title', 'path', 'date', exception_view_link)
    search_fields = ('title',)

    def exception_content_views(self, request, object_id, extra_context=None):
        object = get_object_or_404(ProjectException, id=object_id)
        return render_to_response(
                                  'view_handled_exception.html',
                                  {'contents': object.contents}
                                  )

    def get_urls(self):
        urls = super(ProjectExceptionAdmin, self).get_urls()
        additional_urls = patterns('',
            url(
                r'^(?P<object_id>.+)/view/$',
                self.admin_site.admin_view(self.exception_content_views),
                name="exception_content_views"
                ),
        )
        return additional_urls + urls

admin.site.register(ProjectException, ProjectExceptionAdmin)

