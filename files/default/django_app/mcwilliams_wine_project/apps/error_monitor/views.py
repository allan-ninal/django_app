from .models import ProjectException

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.admin.views.decorators import staff_member_required


@staff_member_required
def view_handled_exception(request, exception_id):
    exception = ProjectException.objects.get(id = exception_id)
    return render_to_response('view_handled_exception.html',
        {"content": exception.contents},
        context_instance=RequestContext(request)
    )

