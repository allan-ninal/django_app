from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = (
        u"Removes 2 minutes old temp files."
    )

    def handle(self, *args, **options):
        import os
        import datetime

        from django.conf import settings

        from mcwilliams_wine_project.apps.web.functions import modification_date

        timedelta_in_seconds = 120
        now = datetime.datetime.utcnow()
        path = settings.BASE_DIR + '/mcwilliams_wine_project/static/tmp/'

        all_files = os.listdir(path)

        counter = 0
        for file in all_files:

            full_path = path + file

            if (now - modification_date(full_path)).seconds > timedelta_in_seconds:
                counter += 1
                os.remove(full_path)


        self.stdout.write('Successfully removed %i files.' % counter)
