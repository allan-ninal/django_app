from django import template

register = template.Library()

@register.simple_tag
def random_number():
    return str(1.21)