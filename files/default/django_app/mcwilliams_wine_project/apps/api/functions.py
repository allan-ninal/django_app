from django.conf import settings
from django.http import HttpResponse, JsonResponse
from functools import wraps

import datetime

from mcwilliams_wine_project.apps.db.models import SlideElement


def ajax_request_api(func):
    """
    If view returned serializable dict, returns JsonResponse with this dict as content.

    example:

        @ajax_request
        def my_view(request):
            news = News.objects.all()
            news_titles = [entry.title for entry in news]
            return {'news_titles': news_titles}
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):

        response = func(request, *args, **kwargs)

        if isinstance(response, dict):

            status_code = 200

            if not response.get('success'):
                status_code = 400

            http_response = JsonResponse(response)
            http_response['Content-Length'] = len(http_response.content)
            http_response.status_code = status_code

            return http_response
        else:
            return response

    return wrapper


def ajax_request_and_auth_api(func):
    """
    If view returned serializable dict, returns JsonResponse with this dict as content.

    example:

        @ajax_request
        def my_view(request):
            news = News.objects.all()
            news_titles = [entry.title for entry in news]
            return {'news_titles': news_titles}
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):

        if request.user.id is None:

            response = {'success': False, 'error': 'Not authenticated'}
            status_code = 401

            http_response = JsonResponse(response)
            http_response['Content-Length'] = len(http_response.content)
            http_response.status_code = status_code

            return http_response

        response = func(request, *args, **kwargs)

        if isinstance(response, dict):

            status_code = 200

            if not response.get('success'):
                status_code = 400

            http_response = JsonResponse(response)
            http_response['Content-Length'] = len(http_response.content)
            http_response.status_code = status_code

            return http_response
        else:
            return response

    return wrapper


def get_user_info(user_obj):

    return {
        'id': user_obj.id,
        'email': user_obj.email,
        'first_name': user_obj.first_name,
        'surname': user_obj.surname,
        # 'date_of_birth': user_obj.date_of_birth.strftime(settings.API_DATE_FORMAT) if user_obj.date_of_birth else '',
        'phone': user_obj.phone,
    }


def get_brand_info(brand_obj):

    return {
        'id': brand_obj.id,
        'name': brand_obj.title
    }


def get_range_info(range_obj):

    return {
        'id': range_obj.id,
        'name': range_obj.title,
    }


def get_sku_info(sku_obj):

    return {
        'id': sku_obj.id,
        'wine_title': sku_obj.title,
        'sku_num': sku_obj.sku_num,
        'vintage_num': sku_obj.vintage_num,
    }


def get_presentation_info(presentation_obj, domain='', pricelists={}):

    from mcwilliams_wine_project.apps.web.functions import encode_url

    thumbnail_url = domain + settings.STATIC_URL + 'images/placeholders/slide/play-video.png'
    present_slides = presentation_obj.slides.through.objects.filter(presentation=presentation_obj)
    slide_list = []

    for index, present_slide in enumerate(present_slides):

        slide = present_slide.slide

        slide_thumbnail_url = domain + encode_url(slide.file.url)

        # Use images only as a presentation preview image otherwise use a one from static dir.
        if slide_thumbnail_url.split('.')[-1].lower() in settings.IMG_EXTS:
            thumbnail_url = slide_thumbnail_url

        slide_info = {
            'index': index + 1,
            'file': slide_thumbnail_url,
            'name': slide.title,
            'id': slide.id
        }

        if slide.brand:
            slide_info['brand'] = get_brand_info(slide.brand)

        if slide.range:
            slide_info['range'] = get_range_info(slide.range)

        if slide.sku:
            slide_info['sku'] = get_sku_info(slide.sku)

        # if slide has "button" elements
        buttons = SlideElement.objects.filter(
            slide=slide,
            layout_element__content_type=4
        )
        if buttons:

            buttons_list = []
            for button in buttons:

                buttons_list.append({
                    'x': button.x,
                    'y': button.y,
                    'width': button.width,
                    'height': button.height,
                    'image': domain + encode_url(button.file) if button.file else '',
                    'action': domain + encode_url(button.action),
                })
            slide_info['buttons'] = buttons_list

        # if slide has "text_placeholder" elements
        text_placeholders = SlideElement.objects.select_related(
            'layout_element__text_placeholder_type'
        ).filter(
            slide=slide,
            layout_element__content_type=5
        )

        if text_placeholders:

            text_placeholders_list = []
            for text_placeholder in text_placeholders:
                text_placeholders_list.append({
                    'x': text_placeholder.x,
                    'y': text_placeholder.y,
                    'width': text_placeholder.width,
                    'height': text_placeholder.height,
                    'type': text_placeholder.layout_element.text_placeholder_type,
                })
            slide_info['text_placeholders'] = text_placeholders_list

        slide_list.append(slide_info)

    # pricelist_url = ''
    # #
    # if presentation_obj.state and presentation_obj.state.price_list:
    #     pricelist_url = domain + encode_url(presentation_obj.state.price_list.url)

    return {
        'id': presentation_obj.id,
        'name': presentation_obj.title,
        'thumbnail': domain + encode_url(presentation_obj.thumbnail.url) if presentation_obj.thumbnail else thumbnail_url,
        'description': presentation_obj.description,
        'download_url': domain + encode_url(presentation_obj.archive.url) if presentation_obj.archive else '',
        'date_edited': presentation_obj.date_edited.strftime(settings.API_DATETIME_FORMAT),
        'date_created': presentation_obj.date_created.strftime(settings.API_DATETIME_FORMAT),
        'slides': slide_list,
        'pricelist_url': pricelists[presentation_obj.id] if pricelists else ''
    }