from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout

from django.core.mail import send_mail
from django.conf import settings

import json
import datetime
import pytz
import urllib

from mcwilliams_wine_project.apps.db.models import User, AppLogging, Brand, Range, SKU, Presentation, Slide, Pricelist

from mcwilliams_wine_project.apps.api.forms import UserApiLoginForm

from mcwilliams_wine_project.apps.api.functions import get_user_info, ajax_request_api, ajax_request_and_auth_api, \
    get_brand_info, get_range_info, get_sku_info, get_presentation_info

from mcwilliams_wine_project.apps.functions import get_visible_brands
from mcwilliams_wine_project.apps.general_functions.functions import get_object_or_None


@csrf_exempt
@ajax_request_api
def log_in(request):

    form = UserApiLoginForm(request.POST)

    if form.is_valid():

        email = form.cleaned_data['email']
        password = form.cleaned_data['password']

        user_auth = authenticate(email=email, password=password)
        if user_auth:
            login(request, user_auth)

            return {'success': True, 'user': get_user_info(user_auth)}

        return {'success': False, 'errors': 'Invalid credentials.'}

    else:
        return {'success': False, 'errors': form.errors}


@csrf_exempt
@ajax_request_and_auth_api
def log_create(request):

    text = request.POST.get('text')
    if text:
        AppLogging.objects.create(text=text)

    return {'success': True}


@csrf_exempt
@ajax_request_and_auth_api
def user_profile_edit(request):

    user = request.user
    data = request.POST

    new_password = data.get('new_password')
    password = data.get('password')
    first_name = data.get('first_name')
    surname = data.get('surname')
    email = data.get('email')
    phone = data.get('phone')

    do_save = False

    if not password:
        return {'success': False, 'errors': '"password" parameter not found'}

    if not user.check_password(password):
        return {'success': False, 'errors': 'Password invalid'}

    if new_password:
        user.set_password(new_password)
        user.save()

        subject = "Password Reset"
        message = """Dear %s,

Your password has been changed.

Best Regards,
McWilliam's team
        """ % user.first_name

        from_email = str(settings.EMAIL_HOST_USER)
        to_email = [str(user.email)]
        # try:
        send_mail(subject, message, from_email, to_email, fail_silently=True)
        # except:
        #     pass

    if first_name:
        user.first_name = first_name
        do_save = True

    if surname:
        user.surname = surname
        do_save = True

    if email:
        user.email = email
        do_save = True

    if phone:
        user.phone = phone
        do_save = True

    if do_save:
        user.save()

    return {'success': True, 'user': get_user_info(user)}


@csrf_exempt
@ajax_request_and_auth_api
def brands(request):

    brands = Brand.objects.all().order_by('title')
    brand_list = []

    for brand in brands:
        brand_list.append(get_brand_info(brand))

    return {'success': True, 'brands': brand_list}


@csrf_exempt
@ajax_request_and_auth_api
def ranges(request):

    brand_id = request.GET.get('brand_id')

    ranges = Range.objects.all().order_by('title')
    if brand_id:
        ranges = ranges.filter(brand_id=brand_id)

    range_list = []

    for range in ranges:
        range_list.append(get_range_info(range))

    return {'success': True, 'ranges': range_list}


@csrf_exempt
@ajax_request_and_auth_api
def skus(request):

    range_id = request.GET.get('range_id')

    skus = SKU.objects.all().order_by('title')
    if range_id:
        skus = skus.filter(range_id=range_id)

    sku_list = []

    for sku in skus:
        sku_list.append(get_sku_info(sku))

    return {'success': True, 'skus': sku_list}


@csrf_exempt
@ajax_request_and_auth_api
def presentations(request):

    user = request.user

    domain = ''
    if not settings.USE_S3:
        domain = request.META['wsgi.url_scheme'] + '://' + request.META['HTTP_HOST']

    brand_id = request.GET.get('brand_id')
    range_id = request.GET.get('range_id')
    sku_id = request.GET.get('sku_id')
    existing_presentations = request.GET.get('existing_presentations', '{}')

    presentations = Presentation.objects.all().order_by('title')

    if user.role == User.END_USER:

        # get price list logic
        my_distributor = user.distributor
        my_state = user.state
        my_country = user.country

        if my_state:
            my_pricelist = get_object_or_None(Pricelist, state=my_state, distributor=my_distributor)
        else:
            my_pricelist = get_object_or_None(Pricelist, country=my_country, distributor=my_distributor)

        presentations = presentations.filter(distributors=my_distributor)

    if existing_presentations:

        try:
            existing_presentations = json.loads(existing_presentations)
        except:
           pass

    if brand_id:
        presentations = presentations.filter(slides__brand_id=brand_id)
    if range_id:
        presentations = presentations.filter(slides__range_id=range_id)
    if sku_id:
        presentations = presentations.filter(slides__sku_id=sku_id)

    presentations = presentations.distinct()

    date_edited_of_presentation = {}
    for presentation in presentations:
        date_edited_of_presentation[presentation.id] = presentation.date_edited

    presentation_list = []

    for presentation in presentations:

        presentation_info = get_presentation_info(presentation, domain)
        presentation_id = str(presentation.id)

        # has updates logic
        has_updates = False
        if presentation_id in existing_presentations:

            date_edited_from_ios = datetime.datetime.strptime(existing_presentations[presentation_id], settings.API_DATETIME_FORMAT).replace(tzinfo=pytz.utc)
            if date_edited_from_ios < presentation.date_edited.replace(microsecond=0):
                has_updates = True

        presentation_info['has_updates'] = has_updates
        presentation_info['pricelist_url'] = ''

        if user.role == User.END_USER:
            if my_pricelist and my_pricelist.file:
                presentation_info['pricelist_url'] = my_pricelist.file.url

        presentation_list.append(presentation_info)

    return {'success': True, 'presentations': presentation_list}


@csrf_exempt
@ajax_request_api
def slideshow_date_edited(request):

    slideshow_id = request.GET.get('slideshow_id')
    presentation = get_object_or_None(Presentation, id=slideshow_id)

    if presentation:
        return {'success': True, 'date': presentation.date_edited.replace(tzinfo=pytz.utc).strftime(settings.API_DATETIME_FORMAT)}

    return {'success': False, 'errors': 'Presentation does not exist'}

