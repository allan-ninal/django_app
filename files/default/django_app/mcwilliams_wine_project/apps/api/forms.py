from django import forms
from django.forms import Form, ModelForm
from distutils.version import LooseVersion

from mcwilliams_wine_project.apps.db.models import User, AppVersion


class UserApiLoginForm(Form):

    email = forms.EmailField()
    password = forms.CharField()
    ios_version = forms.CharField(required=False)

    def clean_ios_version(self):

        ios_version = self.cleaned_data['ios_version']

        app_version_from_db = AppVersion.objects.filter(type=AppVersion.IOS_APP).last()
        if app_version_from_db:
            app_version_from_db = app_version_from_db.value

            version_passable = LooseVersion(app_version_from_db) <= LooseVersion(ios_version)
            if not version_passable:
                raise forms.FieldError('Version is not up to date')

        return ios_version