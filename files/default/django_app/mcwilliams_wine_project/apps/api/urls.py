from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('mcwilliams_wine_project.apps.api.views',

    url(r'^login/$', 'log_in', name='api_login'),

    url(r'^log_create/$', 'log_create', name='api_log_create'),

    url(r'^user_profile_edit/$', 'user_profile_edit', name='api_user_profile_edit'),

    url(r'^brands/$', 'brands', name='brands'),
    url(r'^ranges/$', 'ranges', name='ranges'),
    url(r'^skus/$', 'skus', name='skus'),
    url(r'^presentations/$', 'presentations', name='presentations'),

    url( r'^slideshow_date_edited/$', 'slideshow_date_edited', name='slideshow_date_edited'),

)

