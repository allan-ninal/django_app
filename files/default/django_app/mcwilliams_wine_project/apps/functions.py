from mcwilliams_wine_project.apps.db.models import Brand


def get_visible_brands(user):

    brands = Brand.objects.all()

    if user.distributor:
        brands = user.distributor.brands.all()

    return brands