from django import forms
from django.forms import Form, ModelForm

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _

from mcwilliams_wine_project.apps.db.models import User, Country, Distributor, Brand, Range, SKU, Slide, Presentation, \
    State, Pricelist


class UserAddSaveForm(ModelForm):

    user_id = forms.IntegerField(required=False)
    password = forms.CharField(required=False)
    date_of_birth = forms.DateTimeField(input_formats=['%d/%m/%Y'], required=False)

    class Meta:
        model = User
        fields = (
            'email', 'first_name', 'surname', 'password', 'role', 'phone', 'distributor', 'country', 'state'
        )

    def clean_email(self):

        email = self.cleaned_data['email']
        user_id = self.data.get('user_id')

        users = User.objects.filter(email__iexact=email)

        if user_id:
            users = users.exclude(id=user_id)

        if users.exists():
            raise ValidationError(_('Email is already taken'), code='invalid')

        return email


class UserIdForm(Form):

    user_id = forms.IntegerField()


class CountryAddSaveForm(ModelForm):

    country_id = forms.IntegerField(required=False)

    class Meta:
        model = Country
        fields = '__all__'

    def clean_title(self):

        title = self.cleaned_data['title']
        country_id = self.data.get('country_id')

        countries = Country.objects.filter(title__iexact=title)

        if country_id:
            countries = countries.exclude(id=country_id)

        if countries.exists():
            raise ValidationError(_('Country title is already taken'), code='invalid')

        return title


class DistributorAddSaveForm(ModelForm):

    distributor_id = forms.IntegerField(required=False)

    class Meta:
        model = Distributor
        fields = '__all__'

    def clean_title(self):

        title = self.cleaned_data['title']
        distributor_id = self.data.get('distributor_id')

        distributors = Distributor.objects.filter(title__iexact=title)

        if distributor_id:
            distributors = distributors.exclude(id=distributor_id)

        if distributors.exists():
            raise ValidationError(_('Distributor title is already taken'), code='invalid')

        return title


class BrandAddSaveForm(ModelForm):

    brand_id = forms.IntegerField(required=False)

    class Meta:
        model = Brand
        fields = '__all__'

    def clean_title(self):

        title = self.cleaned_data['title']
        brand_id = self.data.get('brand_id')

        brands = Brand.objects.filter(title__iexact=title)

        if brand_id:
            brands = brands.exclude(id=brand_id)

        if brands.exists():
            raise ValidationError(_('Brand title is already taken'), code='invalid')

        return title


class RangeAddSaveForm(ModelForm):

    range_id = forms.IntegerField(required=False)

    class Meta:
        model = Range
        fields = '__all__'

    def clean_title(self):

        title = self.cleaned_data['title']
        range_id = self.data.get('range_id')
        brand_id = self.data.get('brand')

        ranges = Range.objects.filter(brand_id=brand_id, title__iexact=title)

        if range_id:
            ranges = ranges.exclude(id=range_id)

        if ranges.exists():
            raise ValidationError(_('Range title is already taken'), code='invalid')

        return title


class SKUAddSaveForm(ModelForm):

    sku_id = forms.IntegerField(required=False)

    class Meta:
        model = SKU
        fields = '__all__'

    def clean_title(self):

        title = self.cleaned_data['title']
        sku_id = self.data.get('sku_id')

        skus = SKU.objects.filter(title__iexact=title)

        if sku_id:
            skus = skus.exclude(id=sku_id)

        if skus.exists():
            raise ValidationError(_('SKU title is already taken'), code='invalid')

        return title


class SlideAddSaveForm(ModelForm):

    slide_id = forms.IntegerField(required=False)

    class Meta:
        model = Slide
        exclude = ('user', 'file', 'file_thumbnail')


class PresentationAddSaveForm(ModelForm):

    # slide_id = forms.IntegerField(required=False)
    thumbnail_url = forms.CharField(required=False)

    class Meta:
        model = Presentation
        exclude = ('user', 'slides')


class StateAddViewForm(ModelForm):

    class Meta:
        model = State
        fields = '__all__'


class PriceListForm(ModelForm):

    # def __init__(self, *args, **kwargs):
        # self.bind_state_id = kwargs.pop('bind_state_id', None)
        # super(PriceListForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Pricelist
        fields = ('file', )

    def add_prefix(self, field_name):
        # look up field name; return original if not found
        state_id_str = ''
        state_id_ = self.initial.pop('state_id_', '')
        if state_id_:
            state_id_str = '--' + state_id_
        # print field_name
        return super(PriceListForm, self).add_prefix(state_id_str)