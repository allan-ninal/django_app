import json
import urllib2
import os

from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.db.models import Q

from mcwilliams_wine_project.apps.web.functions import render_to, ajax_request, encode_url
from mcwilliams_wine_project.apps.general_functions.functions import get_object_or_None
from mcwilliams_wine_project.apps.db.models import User, Asset, TempPreviewImage, Slide, Layout, Font, Brand, Range, \
    SKU, SlideElement, Country, Distributor, State, Pricelist
from mcwilliams_wine_project.apps.web.forms import UserAddSaveForm, PriceListForm

from easy_thumbnails.files import get_thumbnailer

from django.conf import settings


@csrf_exempt
@ajax_request
def user_add_save(request):

    user_id = request.POST.get('user_id')

    user = None

    if user_id:
        user = get_object_or_None(User, id=user_id)
        old_password = user.password

    form = UserAddSaveForm(request.POST, instance=user)

    if form.is_valid():

        user_instance = form.save(commit=False)

        password = form.cleaned_data['password']

        #
        if not user or (user and password):
            user_instance.set_password(password)

        else:
            user_instance.password = old_password

        user_instance.save()

        return {'success': True}

    else:

        return {'success': False, 'errors': 'Email is already taken. Please choose another one.', 'errors_dict': form.errors}


@ajax_request
def slide_elements_view(request):

    fonts = Font.objects.all()

    slide_id = request.GET.get('slide_id')

    # Do edit of slide
    if slide_id:
        elements = []

        layout_id = request.GET.get('layout')
        layout = get_object_or_None(Layout, id=layout_id)
        layout_elements = layout.elements.all().order_by('order_number')

        layout_id_of_slide_element = {}
        slide_elements = SlideElement.objects.filter(slide_id=slide_id).order_by('layout_element__order_number')

        for slide_element in slide_elements:
            layout_id_of_slide_element[slide_element.layout_element_id] = slide_element

        for layout_element in layout_elements:

            if layout_element.id in layout_id_of_slide_element:
                elements.append(layout_id_of_slide_element[layout_element.id])
            else:
                elements.append(layout_element)

    # Creation of slide
    else:

        layout_id = request.GET.get('layout')
        layout = get_object_or_None(Layout, id=layout_id)
        elements = layout.elements.all().order_by('order_number')


    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_elements.html',
        {
            'elements': elements,
            'fonts': fonts
        }
    )

    return {'success': True, 'html_content': html_content}


@ajax_request
def slide_asset_list(request):

    id = request.GET.get('id')
    type = request.GET.get('type', 'brands')
    subtype = request.GET.get('subtype', '')

    trees = []
    prev_type = ''
    prev_id = ''
    type_list = ['brands', 'ranges', 'skus', 'within_sku']
    subtypes = ['documents', 'images']

    next_type = type_list[(type_list.index(type) + 1) % len(type_list)]

    assets = Asset.objects.all().order_by('-date_created')

    if type == 'brands':
        trees = Brand.objects.all().order_by('title')
        assets = assets.none()

    elif type == 'ranges':
        trees = Range.objects.filter(brand_id=id).order_by('title')
        assets = assets.filter(brand_id=id)

        prev_type = 'brands'
        prev_id = ''

    elif type == 'skus':
        trees = SKU.objects.filter(range_id=id).order_by('title')
        assets = assets.filter(range_id=id)

        prev_type = 'ranges'
        prev_id = Range.objects.get(id=id).brand_id

    elif type == 'within_sku':
        assets = assets.filter(sku_id=id, subtype__icontains=subtype)

        prev_type = 'skus'
        prev_id = SKU.objects.get(id=id).range_id

    if subtype:
        assets = assets.filter(subtype__icontains=subtype)
        try:
            trees = trees.none()
        except:
            trees = []

        prev_type = type
        prev_id = id

    else:
        assets = assets.exclude(
            Q(subtype__icontains=subtypes[0])|
            Q(subtype__icontains=subtypes[1])
        )

    trees_info = []
    for tree in trees:

        tree_info = {
            'title': tree.title,
            'image': "%s%s/%s" % (settings.STATIC_URL, 'images', 'folder.png'),
            'id': tree.id,
            'type': next_type,
            'subtype': ''
        }

        trees_info.append(tree_info)

    # do not add Images and Docs folders to Brands screen
    if type != 'brands' and not subtype:
        for subtype in subtypes:

            trees_info.append({
                'title': subtype.capitalize(),
                'image': "%s%s/%s-%s" % (settings.STATIC_URL, 'images', subtype,'folder.png'),
                'id': id,
                'type': type,
                'subtype': subtype
            })


    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_assets.html',
        {
            'assets': assets,
            'trees': trees_info,
            'prev_type': prev_type,
            'prev_id': prev_id
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def slide_asset_list_tree(request):

    type = request.GET.get('type', 'brands')
    type_list = ['brands', 'ranges', 'skus', 'within_sku']
    subtypes = ['Documents', 'Images']
    tree_info = []

    assets = Asset.objects.all().order_by('-date_created')
    brands = Brand.objects.all().values('title', 'id').order_by('title')

    for brand in brands:

        brand_info = {}

        ranges = Range.objects.filter(brand_id=brand['id']).values('title', 'id').order_by('title')
        for range in ranges:

            range_info = {}
            brand_info[range['title']] = range_info

            skus = SKU.objects.filter(range_id=range['id']).values('title', 'id').order_by('title')
            for sku in skus:

                sku_info = {}
                range_info[sku['title']] = sku_info

                for subtype in subtypes:
                    sku_info[subtype] = Asset.objects.filter(sku_id=sku['id'], file__icontains=subtype)

            for subtype in subtypes:
                range_info[subtype] = Asset.objects.filter(range_id=range['id'], file__icontains=subtype)

        for subtype in subtypes:
            brand_info[subtype] = Asset.objects.filter(brand_id=brand['id'], file__icontains=subtype)

        tree_info.append({
            brand['title']: brand_info
        })

    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_assets_tree.html',
        {
            'assets': assets,
            'tree_info': tree_info,
            'subtypes': subtypes
            # 'prev_type': prev_type,
            # 'prev_id': prev_id
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def asset_list_tree(request):

    subtypes = [
        SKU(pk=-1, title='Documents'),
        SKU(pk=-2, title='Images'),
    ]
    tree_info = []

    assets = Asset.objects.all().order_by('-date_created')
    brands = Brand.objects.all().order_by('title')

    for brand in brands:

        brand_info = {}

        ranges = Range.objects.filter(brand_id=brand.id).order_by('title')
        for range in ranges:

            range_info = {}
            brand_info[range] = range_info

            skus = SKU.objects.filter(range_id=range.id).order_by('title')
            for sku in skus:

                sku_info = {}
                range_info[sku] = sku_info

                for subtype in subtypes:
                    sku_info[subtype] = {}

            for subtype in subtypes:
                range_info[subtype] = {}

        for subtype in subtypes:
            brand_info[subtype] = {}

        tree_info.append({
            brand: brand_info
        })

    html_content = render_to_string(
        'web-admin/pages/asset/blocks/assets_tree.html',
        {
            'assets': assets,
            'tree_info': tree_info,
            'subtypes': subtypes
            # 'prev_type': prev_type,
            # 'prev_id': prev_id
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@csrf_exempt
@ajax_request
def slide_preview(request):

    slide_preview_width = request.POST.get('slide_preview_width')
    slide_preview_height = int(slide_preview_width) * 768 / 1024

    # print slide_preview_width

    screen_rate = float(slide_preview_height) / 768
    fields_to_change = ['content_type']
    media_fields_to_proxy = ['file_url']

    slide_elements = json.loads(request.POST.get('slide_elements'))
    # slide_elements_original = json.loads(request.POST.get('slide_elements'))

    # print slide_elements

    for element_info in slide_elements:

        for key, value in element_info.items():

            if key in fields_to_change:

                # is video
                if value == 3:

                    element_info['video_thumbnail'] = settings.STATIC_URL + 'images/placeholders/slide/play-video.png'

            if settings.USE_S3:

                if key in media_fields_to_proxy and value:

                    s3_url = value
                    img_filename = s3_url.split('/')[-1]
                    img_ext = s3_url.split('.')[-1]

                    img_temp = NamedTemporaryFile(delete=False, dir=settings.STATICFILES_DIRS[0], suffix='.'+img_ext, prefix='tmp/')
                    img_temp.write(urllib2.urlopen(encode_url(s3_url)).read())
                    img_temp.flush()

                    # TempPreviewImage.objects.create(
                    #     path=img_temp.path
                    # )

                    os.chmod(img_temp.name, 0666)

                    # im.file.save(img_filename, File(img_temp))

                    element_info[key] = settings.STATIC_URL + '/'.join(img_temp.name.split('/')[-2:])


    # print slide_elements_original

    # temp_preview_image = get_object_or_None(TempPreviewImage, user=request.user)
    #
    # if temp_preview_image:
    #     temp_preview_image.file = background_url
    #     temp_preview_image.save()
    #
    # else:
    #     TempPreviewImage.objects.create(user=request.user, file=background_url)

    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_preview.html',
        {
            'slide_elements': slide_elements,
            'slide_preview_width': slide_preview_width,
            'slide_preview_height': slide_preview_height,
            # 'slide_elements_original': slide_elements_original,
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def presentation_slide_grid(request):

    type = request.GET.get('type')
    id = request.GET.get('value')

    slides = Slide.objects.all()

    if id:
        if type == 'brand':
            slides = slides.filter(brand_id=id)

        elif type == 'range':
            slides = slides.filter(range_id=id)

        elif type == 'sku':
            slides = slides.filter(sku_id=id)

    html_content = render_to_string(
        'web-admin/pages/presentation/blocks/slides_grid.html',
        {
            'slides': slides
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def slide_layouts_view(request):

    brand_id = request.GET.get('brand_id')

    layouts = Layout.objects.all().order_by('title')

    if brand_id:
        layouts = layouts.filter(brand_id=brand_id)


    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_layouts.html',
        {
            'layouts': layouts
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def slide_asset_upload_popup(request):

    brands = Brand.objects.all().order_by('title')

    html_content = render_to_string(
        'web-admin/pages/slide/blocks/slide_upload_asset.html',
        {
            'request': request,
            'brands': brands
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def state_add_popup(request):

    html_content = render_to_string(
        'web-admin/pages/state/blocks/state_add_popup.html',
        {}
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def state_options(request):

    country_id = request.GET.get('country_id')

    country = get_object_or_None(Country, id=country_id)

    html_content = render_to_string(
        'web-admin/pages/country/blocks/states_options.html',
        {
            'states': country.country_states.all().values('title', 'id')
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def country_and_states_options(request):

    distributor_id = request.GET.get('distributor_id')

    distributor = get_object_or_None(Distributor, id=distributor_id)

    states = []
    country = distributor.country

    if country:
        states = country.country_states.all().order_by('title')

    html_content = render_to_string(
        'web-admin/pages/distributor/blocks/country_and_states_options.html',
        {
            'country': country,
            'states': states
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }


@ajax_request
def distributor_pricelist_forms(request):

    country_id = request.GET.get('country_id')
    state_ids = request.GET.get('state_ids').split(',') if request.GET.get('state_ids') else []
    distributor_id = request.GET.get('distributor_id', 0)

    states = State.objects.filter(id__in=state_ids)

    pricelist_of_state = {}
    merged_pricelist_of_state = {}
    pricelist_of_country = {}
    is_edit = False

    if distributor_id:
        is_edit = True

        pricelists = Pricelist.objects.filter(
            state_id__in=state_ids,
            distributor_id=distributor_id
        )

        for pricelist in pricelists:
            pricelist_of_state[pricelist.state] = pricelist

    for state in states:

        merged_pricelist_of_state[state] = None
        if state in pricelist_of_state:
            merged_pricelist_of_state[state] = pricelist_of_state[state]

    if state_ids:

        html_content = render_to_string(
            'web-admin/pages/distributor/blocks/price_list_form.html',
            {
                'merged_pricelist_of_state': merged_pricelist_of_state,
                'is_edit': is_edit,
                'states': states
            }
        )

    else:

        country_price_list = get_object_or_None(Pricelist, country_id=country_id, distributor_id=distributor_id)
        html_content = render_to_string(
            'web-admin/pages/distributor/blocks/price_list_form_country.html',
            {
                'is_edit': is_edit,
                'country_price_list': country_price_list,
                'country_id': country_id,
                'country': get_object_or_None(Country, id=country_id)
            }
        )

    return {
        'success': True,
        'html_content': html_content
    }


@csrf_exempt
@ajax_request
def distributors_by_country(request):

    country_ids = request.POST.getlist('country_ids[]')
    already_set_distributor_ids = request.POST.getlist('distributor_ids[]')

    distributor_ids_int = []
    for distributor_id_str in already_set_distributor_ids:
        distributor_ids_int.append(int(distributor_id_str))

    countries = Country.objects.filter(id__in=country_ids)
    distributors = Distributor.objects.filter(country__in=countries).order_by('title').distinct()

    html_content = render_to_string(
        'web-admin/pages/presentation/blocks/distributors_by_country.html',
        {
            'distributors': distributors,
            'already_set_distributor_ids': distributor_ids_int
        }
    )

    return {
        'success': True,
        'html_content': html_content
    }