from django.conf.urls import patterns, include, url

urlpatterns = patterns('mcwilliams_wine_project.apps.web.views',

    url(r'^login/$', 'login_view'),

    url(r'^$', 'index'),
    url(r'^dashboard/$', 'dashboard'),

    url(r'^user/add/$', 'user_add_view'),
    url(r'^user/list/$', 'user_list_view'),
    url(r'^user/list/(?P<user_id>\d+)/$', 'user_profile_view'),

    url(r'^country/add/$', 'country_add_view'),
    url(r'^country/list/$', 'country_list_view'),
    url(r'^country/list/(?P<country_id>\d+)/$', 'country_profile_view'),
    url(r'^country/list/(?P<country_id>\d+)/state/add/$', 'state_add_view'),

    url(r'^state/list/(?P<state_id>\d+)/$', 'state_profile_view'),

    url(r'^distributor/add/$', 'distributor_add_view'),
    url(r'^distributor/list/$', 'distributor_list_view'),
    url(r'^distributor/list/(?P<distributor_id>\d+)/$', 'distributor_profile_view'),

    url(r'^brand/add/$', 'brand_add_view'),
    url(r'^brand/list/$', 'brand_list_view'),
    url(r'^brand/list/(?P<brand_id>\d+)/$', 'brand_profile_view'),
    url(r'^brand/list/(?P<brand_id>\d+)/range/add/$', 'range_add_view'),
    url(r'^brand/list/(?P<brand_id>\d+)/sku/add/$', 'sku_add_view'),

    url(r'^range/list/(?P<range_id>\d+)/$', 'range_profile_view'),
    url(r'^sku/list/(?P<sku_id>\d+)/$', 'sku_profile_view'),

    url(r'^asset/add/$', 'asset_add_view'),
    url(r'^asset/list/$', 'asset_list_view'),

    url(r'^slide/add/$', 'slide_add_view'),
    url(r'^slide/list/$', 'slide_list_view'),
    url(r'^slide/list/(?P<slide_id>\d+)/$', 'slide_profile_view'),

    url(r'^presentation/add/$', 'presentation_add_view'),
    url(r'^presentation/list/$', 'presentation_list_view'),
    url(r'^presentation/list/(?P<presentation_id>\d+)/$', 'presentation_profile_view'),
    url(r'^presentation/list/(?P<presentation_id>\d+)/generate_pdf/$', 'presentation_generate_pdf_view'),

    url(r'^logout/$', 'log_out'),


    url(r'^test/$', 'test'),

)

## AJAX WEB URLS

urlpatterns += patterns('mcwilliams_wine_project.apps.web.ajax_views',

    url(r'^user/add/save/$', 'user_add_save'),
    url(r'^user/delete/$', 'user_delete'),
    url(r'^user/list/table/$', 'user_list_table'),
    url(r'^user/check_email_is_free/$', 'user_check_email_is_free'),

    url(r'^country/add/save/$', 'country_add_save'),
    url(r'^country/delete/$', 'country_delete'),
    url(r'^country/list/table/$', 'country_list_table'),

    url(r'^distributor/add/save/$', 'distributor_add_save'),
    url(r'^distributor/delete/$', 'distributor_delete'),
    url(r'^distributor/list/table/$', 'distributor_list_table'),

    url(r'^brand/add/save/$', 'brand_add_save'),
    url(r'^brand/delete/$', 'brand_delete'),
    url(r'^brand/list/table/$', 'brand_list_table'),

    url(r'^range/add/save/$', 'range_add_save'),
    url(r'^range/delete/$', 'range_delete'),
    url(r'^range/list/table/$', 'range_list_table'),

    url(r'^sku/add/save/$', 'sku_add_save'),
    url(r'^sku/delete/$', 'sku_delete'),
    url(r'^sku/list/table/$', 'sku_list_table'),

    url( r'asset/add/save/$', 'asset_add_save', name='jfu_upload'),
    url(r'^asset/list/table/$', 'asset_list_table'),
    url(r'^asset/delete/$', 'asset_delete'),
    url(r'^asset/has_dependencies/$', 'asset_has_dependencies'),
    url(r'^asset/will_override/$', 'asset_will_override'),

    url(r'^slide/add/save/$', 'slide_add_save'),
    url(r'^slide/delete/$', 'slide_delete'),
    url(r'^slide/list/table/$', 'slide_list_table'),
    url(r'^slide/copy/$', 'slide_copy'),

    url(r'^presentation/add/save/$', 'presentation_add_save'),
    url(r'^presentation/list/table/$', 'presentation_list_table'),
    url(r'^presentation/delete/$', 'presentation_delete'),

    url(r'^state/add/save/$', 'state_add_save'),
    url(r'^state/list/table/$', 'state_list_table'),
    url(r'^state/delete/$', 'state_delete'),

    # You may optionally define a delete url as well
    url( r'^delete/(?P<pk>\d+)$', 'upload_delete', name='jfu_delete'),

    url( r'^get_zip/$', 'get_zip', name='get_zip'),

)


## AJAX BLOCKS URLS

urlpatterns += patterns('mcwilliams_wine_project.apps.web.block_views',

    url(r'^slide/elements/view/$', 'slide_elements_view'),
    # url(r'^slide/asset/list/$', 'slide_asset_list'),
    url(r'^slide/asset/list/$', 'slide_asset_list_tree'),
    url(r'^slide/asset/preview/$', 'slide_preview'),
    url(r'^slide/layouts/view/$', 'slide_layouts_view'),
    url(r'^slide/asset/upload/popup/$', 'slide_asset_upload_popup'),

    url(r'^presentation/slide/grid/$', 'presentation_slide_grid'),
    url(r'^presentation/distributors_by_country/$', 'distributors_by_country'),

    url(r'^state/add/popup/$', 'state_add_popup'),
    url(r'^state/options/$', 'state_options'),

    url(r'^asset/list/tree/$', 'asset_list_tree'),

    url(r'^user/country_and_states_options/$', 'country_and_states_options'),

    url(r'^distributor/pricelist_forms/$', 'distributor_pricelist_forms'),

)


