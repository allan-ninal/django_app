import zipfile
import os
import json
import datetime
import hashlib
import urllib2

from django.core.files.temp import NamedTemporaryFile
from django.core.files import File
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models import F

from mcwilliams_wine_project.apps.db.models import Brand, Asset, Country

from functools import wraps


def render_to(template=None, mimetype="text/html"):
    """
    Decorator for Django views that sends returned dict to render_to_response
    function.

    Template name can be decorator parameter or TEMPLATE item in returned
    dictionary.  RequestContext always added as context instance.
    If view doesn't return dict then decorator simply returns output.

    Parameters:
     - template: template name to use
     - mimetype: content type to send in response headers

    Examples:
    # 1. Template name in decorator parameters

    @render_to('template.html')
    def foo(request):
        bar = Bar.object.all()
        return {'bar': bar}

    # equals to
    def foo(request):
        bar = Bar.object.all()
        return render_to_response('template.html',
                                  {'bar': bar},
                                  context_instance=RequestContext(request))


    # 2. Template name as TEMPLATE item value in return dictionary.
         if TEMPLATE is given then its value will have higher priority
         than render_to argument.

    @render_to()
    def foo(request, category):
        template_name = '%s.html' % category
        return {'bar': bar, 'TEMPLATE': template_name}

    #equals to
    def foo(request, category):
        template_name = '%s.html' % category
        return render_to_response(template_name,
                                  {'bar': bar},
                                  context_instance=RequestContext(request))

    """
    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            output = function(request, *args, **kwargs)
            if not isinstance(output, dict):
                return output
            tmpl = output.pop('TEMPLATE', template)
            return render_to_response(tmpl, output,\
                context_instance=RequestContext(request))
        return wrapper
    return renderer


def ajax_request(func):
    """
    If view returned serializable dict, returns JsonResponse with this dict as content.

    example:

        @ajax_request
        def my_view(request):
            news = News.objects.all()
            news_titles = [entry.title for entry in news]
            return {'news_titles': news_titles}
    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):

        response = func(request, *args, **kwargs)

        if isinstance(response, dict):

            http_response = JsonResponse(response)
            http_response['Content-Length'] = len(http_response.content)

            return http_response
        else:
            return response

    return wrapper


def get_brands(func):
    """
    Fetches all brands for left sidebar Brands nav to be rendered.

    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):

        response = func(request, *args, **kwargs)

        if isinstance(response, dict):

            response['brand_list'] = Brand.objects.all().values('title', 'id')

            return response
        else:
            return response

    return wrapper


def get_countries(func):
    """
    Fetches all countries for left sidebar Country nav to be rendered.

    """
    @wraps(func)
    def wrapper(request, *args, **kwargs):

        response = func(request, *args, **kwargs)

        if isinstance(response, dict):

            response['country_list'] = Country.objects.all().values('title', 'id')

            return response
        else:
            return response

    return wrapper


# def gen_slide_archive(name='zipfile_write', presentation_obj=None):
#
#     # slides = Slide.objects.filter(presentation=presentation_obj)
#
#     dirname = os.path.dirname(files_url[0])
#     archive_path = dirname + name + '.zip'
#
#     zf = zipfile.ZipFile(archive_path, mode='w')
#     for file_path in files_url:
#
#         try:
#             zf.write(file_path, arcname=os.path.basename(file_path))
#         except:
#             pass
#
#     zf.close()
#
#     return archive_path


# msg = 'This data did not exist in a file before being added to the ZIP file'
# zf = zipfile.ZipFile('zipfile_writestr.zip',
#                      mode='w',
#                      compression=zipfile.ZIP_DEFLATED,
#                      )
# try:
#     zf.writestr('from_string.txt', msg)
# finally:
#     zf.close()
#
# print_info('zipfile_writestr.zip')
#
# zf = zipfile.ZipFile('zipfile_writestr.zip', 'r')
# print zf.read('from_string.txt')


def gen_presentation_archive(presentation_obj):
    from mcwilliams_wine_project.apps.db.models import SlideElement
    from mcwilliams_wine_project.apps.api.functions import get_brand_info, get_range_info, get_sku_info
    from django.conf import settings

    archive_path = settings.MEDIA_ROOT + '/zipfile_writestr.zip'
    present_slides = presentation_obj.slides.through.objects.filter(presentation=presentation_obj)
    slide_list = []
    slide_with_image_found = False

    zf = zipfile.ZipFile(
        archive_path,
        mode='w',
        compression=zipfile.ZIP_DEFLATED
    )
    presentation_file_path = settings.STATICFILES_DIRS[0] + '/images/placeholders/slide/play-video.png'
    presentation_file_name = 'play-video.png'

    for index, present_slide in enumerate(present_slides):

        slide = present_slide.slide

        slide_file_path = settings.BASE_DIR + '/mcwilliams_wine_project' + slide.file.url
        slide_file_name = os.path.basename(slide.file.url)

        zf.write(slide_file_path, arcname=slide_file_name)

        if slide_file_name.split('.')[-1].lower() in settings.IMG_EXTS:
            presentation_file_path = slide_file_path
            presentation_file_name = slide_file_name
            slide_with_image_found = True

        slide_info = {
            'index': index + 1,
            'file': slide_file_name,
            'name': slide.title,
            'id': slide.id
        }

        if slide.brand:
            slide_info['brand'] = get_brand_info(slide.brand)

        if slide.range:
            slide_info['range'] = get_range_info(slide.range)

        if slide.sku:
            slide_info['sku'] = get_sku_info(slide.sku)

        # if slide has "button" elements
        buttons = SlideElement.objects.filter(
            slide=slide,
            layout_element__content_type=4
        )
        if buttons:

            buttons_list = []
            for button in buttons:
                buttons_list.append({
                    'x': button.x,
                    'y': button.y,
                    'width': button.width,
                    'height': button.height,
                    'image': os.path.basename(button.file.url),
                    'action': button.action.split('/')[-1]
                })

                full_path = settings.BASE_DIR + '/mcwilliams_wine_project' + button.file.url
                zf.write(full_path, arcname=os.path.basename(full_path))

                if button.action:
                    slide_action_path = settings.BASE_DIR + '/mcwilliams_wine_project' + button.action
                    slide_action_name = button.action.split('/')[-1]

                    zf.write(slide_action_path, arcname=slide_action_name)

            slide_info['buttons'] = buttons_list

        # if slide has "text_placeholder" elements
        text_placeholders = SlideElement.objects.select_related(
            'layout_element__text_placeholder_type'
        ).filter(
            slide=slide,
            layout_element__content_type=5
        )

        if text_placeholders:

            text_placeholders_list = []
            for text_placeholder in text_placeholders:
                text_placeholders_list.append({
                    'x': text_placeholder.x,
                    'y': text_placeholder.y,
                    'width': text_placeholder.width,
                    'height': text_placeholder.height,
                    'type': text_placeholder.layout_element.text_placeholder_type,
                })
            slide_info['text_placeholders'] = text_placeholders_list

        slide_list.append(slide_info)

    # Avoid dbl saving image of slide as slide_thumb and presentation_thumb
    if not slide_with_image_found:
        zf.write(presentation_file_path, arcname=os.path.basename(presentation_file_name))

    response = json.dumps({
        # 'id': presentation_obj.id,
        # 'name': presentation_obj.title,
        'thumbnail': presentation_file_name,
        # 'description': presentation_obj.description,
        # 'download_url': '',
        'slides': slide_list
    })

    try:
        zf.writestr('contents.json', response)
    finally:
        zf.close()

    return archive_path


def encode_url(url):

    import urllib

    return urllib.quote(url, '://')


def process_url(url):
    import urllib2
    from django.conf import settings

    try:
        bin_data = urllib2.urlopen(encode_url(url)).read()
    except:
        print "URL BROKEN - %s" % url

        url = settings.CURRENT_DOMAIN + '/static/images/placeholders/slide/play-video.png'
        bin_data = urllib2.urlopen(encode_url(url)).read()

    return bin_data, url.split('/')[-1]


def gen_presentation_archive_s3(presentation_obj):

    from mcwilliams_wine_project.apps.db.models import SlideElement
    from mcwilliams_wine_project.apps.api.functions import get_brand_info, get_range_info, get_sku_info
    from django.conf import settings
    import urllib2

    archive_path = settings.MEDIA_ROOT + '/zipfile_writestr.zip'
    present_slides = presentation_obj.slides.through.objects.filter(presentation=presentation_obj)
    slide_list = []
    slide_with_image_found = False
    presentation_has_own_thumbnail = False

    zf = zipfile.ZipFile(
        archive_path,
        mode='w',
        compression=zipfile.ZIP_DEFLATED
    )
    presentation_file_path = settings.STATICFILES_DIRS[0] + '/images/placeholders/slide/play-video.png'
    presentation_file_name = 'play-video.png'

    if presentation_obj.thumbnail:
        presentation_file_path = urllib2.urlopen(encode_url(presentation_obj.thumbnail.url)).read()
        presentation_file_name = presentation_obj.thumbnail.name.split('/')[-1]
        presentation_has_own_thumbnail = True

    for index, present_slide in enumerate(present_slides):

        slide = present_slide.slide

        slide_file_path, slide_file_name = process_url(encode_url(slide.file.url))

        # zf.write(slide_file_path, arcname=slide_file_name)
        zf.writestr(slide_file_name, slide_file_path)

        if slide_file_name.split('.')[-1].lower() in settings.IMG_EXTS and not presentation_has_own_thumbnail:
            presentation_file_path = slide_file_path
            presentation_file_name = slide_file_name
            slide_with_image_found = True

        slide_info = {
            'index': index + 1,
            'file': slide_file_name,
            'name': slide.title,
            'id': slide.id
        }

        if slide.brand:
            slide_info['brand'] = get_brand_info(slide.brand)

        if slide.range:
            slide_info['range'] = get_range_info(slide.range)

        if slide.sku:
            slide_info['sku'] = get_sku_info(slide.sku)

        # if slide has "button" elements
        buttons = SlideElement.objects.filter(
            slide=slide,
            layout_element__content_type=4
        )
        if buttons:

            buttons_list = []
            for button in buttons:
                buttons_list.append({
                    'x': button.x,
                    'y': button.y,
                    'width': button.width,
                    'height': button.height,
                    'image': button.file.split('/')[-1],
                    'action': button.action.split('/')[-1]
                })

                full_path = urllib2.urlopen(encode_url(button.file)).read()
                # zf.write(full_path, arcname=button.file.url.split('/')[-1])
                zf.writestr(button.file.split('/')[-1], full_path)

                if button.action:
                    slide_action_path = urllib2.urlopen(encode_url(button.action)).read()
                    slide_action_name = button.action.split('/')[-1]

                    # zf.write(slide_action_path, arcname=slide_action_name)
                    zf.writestr(slide_action_name, slide_action_path)

            slide_info['buttons'] = buttons_list

        # if slide has "text_placeholder" elements
        text_placeholders = SlideElement.objects.select_related(
            'layout_element__text_placeholder_type'
        ).filter(
            slide=slide,
            layout_element__content_type=5
        )

        if text_placeholders:

            text_placeholders_list = []
            for text_placeholder in text_placeholders:
                text_placeholders_list.append({
                    'x': text_placeholder.x,
                    'y': text_placeholder.y,
                    'width': text_placeholder.width,
                    'height': text_placeholder.height,
                    'type': text_placeholder.layout_element.text_placeholder_type,
                })
            slide_info['text_placeholders'] = text_placeholders_list

        slide_list.append(slide_info)

    # Avoid dbl saving image of slide as slide_thumb and presentation_thumb
    if not slide_with_image_found:
        # zf.write(presentation_file_path, arcname=presentation_file_name)
        zf.writestr(presentation_file_name, presentation_file_path)

    response = json.dumps({
        # 'id': presentation_obj.id,
        # 'name': presentation_obj.title,
        'thumbnail': presentation_file_name,
        # 'description': presentation_obj.description,
        # 'download_url': '',
        'slides': slide_list
        # 'pricelist': pricelist_name
    })

    try:
        zf.writestr('contents.json', response)
    finally:
        zf.close()

    return archive_path


def modification_date(filename):

    # >>> d = modification_date('/var/log/syslog')
    # >>> print d
    # 2009-10-06 10:50:01
    # >>> print repr(d)
    # datetime.datetime(2009, 10, 6, 10, 50, 1)

    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)


def create_home_dir(obj, parents=[]):

    class_name = obj.__class__.__name__.lower() + 's'

    title = obj.title
    safe_title = "".join(symbol for symbol in title if symbol.isalnum())

    parents_path = ''
    for parent in parents:
        parent_class_name = parent.__class__.__name__.lower() + 's'
        parent_title = parent.title
        parent_safe_title = "".join(symbol for symbol in parent_title if symbol.isalnum())

        parents_path += parent_class_name + '/' + parent_safe_title + '/'

    full_path = "%s%s/%s/" % (parents_path, class_name, safe_title)

    asset = Asset.objects.create(file=ContentFile('root_placeholder_string', full_path + 'root_placeholder.jpg'))
    # asset.file.delete(save = False)
    asset.delete()


import urlparse
from django.conf import settings
from storages.backends.s3boto import S3BotoStorage

def domain(url):
    return urlparse.urlparse(url).hostname

class MediaFilesStorage(S3BotoStorage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket'] = settings.AWS_STORAGE_BUCKET_NAME
        kwargs['custom_domain'] = domain(settings.MEDIA_URL)
        super(MediaFilesStorage, self).__init__(*args, **kwargs)


def get_copy_name(name):

    new_name_array = name.split('_')

    new_name = name
    if len(new_name_array) > 1:
        new_name = new_name_array[0]

    return "%s_copy-%s" % (new_name, hashlib.md5(str(datetime.datetime.now())).hexdigest()[:2])


def get_current_domain(request):
    return request.META['wsgi.url_scheme'] + '://' + request.META['HTTP_HOST']


def generate_pdf(presentation_obj):

    from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
    from reportlab.lib.pagesizes import letter, A4
    from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
    from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

    report_os_path = HttpResponse(content_type='application/pdf')
    report_os_path['Content-Disposition'] = 'attachment; filename="' + '%s.pdf' % get_random_string()

    doc = SimpleDocTemplate(
        report_os_path, pagesize=A4,
        rightMargin=18, leftMargin=18,
        topMargin=30, bottomMargin=70
    )

    Story = []
    presentation_name = presentation_obj.title

    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Header', alignment=TA_CENTER, fontSize=20))

    # BUILD UP PRESENTATION ELEMENTS

    Story.append(Paragraph(presentation_name, styles["Header"]))

    Story.append(Spacer(1, 24))

    slides_relations = presentation_obj.slides.through.objects.filter(presentation=presentation_obj).order_by('id')

    for slide_relation in slides_relations:
        slide = slide_relation.slide
        try:
            if not slide.is_video:
                img_url = slide.file.url

                img_temp = NamedTemporaryFile(delete=False, dir=settings.STATICFILES_DIRS[0], suffix='.jpg', prefix='tmp/')
                img_temp.write(urllib2.urlopen(encode_url(img_url)).read())
                img_temp.flush()

                # Story.append(Image(img_temp.name, width=2*inch, height=1*inch,kind='proportional'))
                Story.append(Image(img_temp.name, width=500, height=760, kind='proportional'))

                Story.append(Spacer(1, 12))
        except:
            pass

    doc.build(Story)

    return report_os_path


def get_random_string(length=30):
    return hashlib.md5(str(datetime.datetime.now())).hexdigest()[:length]


def replace_spaces(str):
    return str.replace(' ', '_').replace(',', '_').replace(';', '_').replace('\'', '_')


def get_asset_future_path(asset_obj, filename):

    new_name = '/assets/%s' % filename

    if asset_obj.brand:
        new_name = '/assets/brands/%s/%s/%s' % (asset_obj.brand.safe_title, asset_obj.subtype, filename, )
    elif asset_obj.range:
        new_name = "/assets/brands/%s/ranges/%s/%s/%s" % (asset_obj.range.brand.safe_title, asset_obj.range.safe_title, asset_obj.subtype, filename, )
    elif asset_obj.sku:
        new_name = "/assets/brands/%s/ranges/%s/skus/%s/%s/%s" % (
            asset_obj.sku.range.brand.safe_title,
            asset_obj.sku.range.safe_title,
            asset_obj.sku.safe_title,
            asset_obj.subtype,
            filename,
        )

    return replace_spaces(new_name)


def set_slideshow_countries_by_country():

    from mcwilliams_wine_project.apps.db.models import Presentation

    for presentation in Presentation.objects.filter(country__isnull=False):
        presentation.countries.add(presentation.country_id)


def get_broken_slides():

    from mcwilliams_wine_project.apps.db.models import SlideElement

    slide_elements = SlideElement.objects.all()

    for slide_element in slide_elements:

        if slide_element.action:

            try:
                urllib2.urlopen(encode_url(slide_element.action))
            except:
                print '=========', slide_element.id, slide_element.slide_id, slide_element.action
