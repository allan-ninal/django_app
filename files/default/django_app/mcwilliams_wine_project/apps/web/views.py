import os

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_POST

from jfu.http import upload_receive, UploadResponse, JFUResponse

from mcwilliams_wine_project.apps.web.functions import render_to, ajax_request, get_brands, get_countries, generate_pdf
from mcwilliams_wine_project.apps.general_functions.functions import get_object_or_None
from mcwilliams_wine_project.apps.db.models import User, Country, Distributor, Brand, Range, SKU, Asset, Layout, \
    LayoutElement, Language, Slide, Presentation, State, Pricelist

from mcwilliams_wine_project.apps.web.forms import StateAddViewForm, DistributorAddSaveForm, PriceListForm


@csrf_exempt
@login_required()
@render_to('web-admin/pages/base.html')
@get_brands
@get_countries
def index(request):
    return HttpResponseRedirect('/slide/list/')


@csrf_exempt
@login_required()
@render_to('web-admin/pages/base.html')
@get_brands
@get_countries
def dashboard(request):
    return {}


@csrf_exempt
@login_required()
@render_to('web-admin/pages/user/user_add.html')
@get_brands
@get_countries
def user_add_view(request):

    user_types = User.ROLE_NO_ADMIN_CHOICES
    distributors = Distributor.objects.all()

    return {
        'user_types': user_types,
        'distributors': distributors
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/user/user_list.html')
@get_brands
@get_countries
def user_list_view(request):

    # user_list = User.objects.all()

    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/user/user_profile.html')
@get_brands
@get_countries
def user_profile_view(request, user_id):

    user = User.objects.get(id=user_id)
    my_country = None
    my_state = None
    my_distributor = None
    country = None
    states = []

    user_info = user.__dict__
    # user_info['date_of_birth'] = user.date_of_birth.strftime(settings.API_DATE_FORMAT) if user.date_of_birth else ''

    distributors = Distributor.objects.all()

    if user.role == User.END_USER:
        my_distributor = user.distributor
        my_country = user.country
        my_state = user.state

        country = my_distributor.country
        states = my_distributor.states.all()

    return {
        'user_current': user_info,
        'user_types': User.ROLE_NO_ADMIN_CHOICES,
        'distributors': distributors,
        'my_country': my_country,
        'my_state': my_state,
        'country': country,
        'states': states,
        'my_distributor': my_distributor
    }


@csrf_exempt
@render_to('web-admin/pages/auth/login.html')
def login_view(request):

    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')

        user_auth = authenticate(email=email, password=password)
        if user_auth and user_auth.role in [User.MASTER_ADMIN, User.CONTENT_MANAGER]:
            login(request, user_auth)

            next_url = request.GET.get('next')
            if next_url:
                return HttpResponseRedirect(next_url)

            return HttpResponseRedirect('/')

        return {
            'success': False
        }

    else:
        return {
            'success': True
        }


@csrf_exempt
def log_out(request):

    logout(request)

    return HttpResponseRedirect(settings.LOGIN_URL)


@csrf_exempt
@login_required()
@render_to('web-admin/pages/country/country_add.html')
@get_brands
@get_countries
def country_add_view(request):

    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/country/country_list.html')
@get_brands
@get_countries
def country_list_view(request):

    # user_list = User.objects.all()

    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/country/country_profile.html')
@get_brands
@get_countries
def country_profile_view(request, country_id):

    country = Country.objects.get(id=country_id)

    return {
        'country': country
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/distributor/distributor_add.html')
@get_brands
@get_countries
def distributor_add_view(request):

    countries = Country.objects.all().order_by('title')

    if request.method == "GET":
        return {
            'countries': countries
        }

    files = request.FILES

    distributor_id = request.POST.get('distributor_id')
    country_id = request.POST.get('country')
    state_ids = request.POST.getlist('states')

    distributor = None

    if distributor_id:
        distributor = get_object_or_None(Distributor, id=distributor_id)


    form = DistributorAddSaveForm(request.POST, instance=distributor)

    if form.is_valid():

        distributor = form.save()

        add_edit_price_list(request.FILES, distributor.id, state_ids, country_id)

        return {'success': True, 'saved': True, 'countries': countries}

    else:

        return {'success': False, 'errors': 'Distributor title is already taken. Please choose another one.', 'errors_dict': form.errors, 'countries': countries}


    # return {
    #     'countries': countries
    # }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/distributor/distributor_list.html')
@get_brands
@get_countries
def distributor_list_view(request):

    # user_list = User.objects.all()

    return {

    }


def pricelist_remove(dct):

    pricelist_ids = []
    for key, value in dct.items():

        if 'clear-pricelist' in key and value == 'on':

            pricelist_ids.append(int(key.split('--')[-1]))

    Pricelist.objects.filter(id__in=pricelist_ids).delete()

    return True


def add_edit_price_list(dct, distributor_id, state_ids, country_id):

    if state_ids:
        print '111111'
        for state_id, file in dct.items():
            obj, created = Pricelist.objects.get_or_create(distributor_id=distributor_id, state_id=state_id)
            obj.file = file
            obj.save()

    else:
        print '222'
        for country_id, file in dct.items():
            obj, created = Pricelist.objects.get_or_create(distributor_id=distributor_id, country_id=country_id)
            obj.file = file
            obj.save()

    return True


@csrf_exempt
@login_required()
@render_to('web-admin/pages/distributor/distributor_profile.html')
@get_brands
@get_countries
def distributor_profile_view(request, distributor_id):

    saved = False

    distributor = Distributor.objects.get(id=distributor_id)
    distributor_country = distributor.country
    distributor_details = distributor.details
    state_ids = request.POST.getlist('states')
    country_id = request.POST.get('country')

    # add/edit pricelist
    add_edit_price_list(request.FILES, distributor_id, state_ids, country_id)

    # remove pricelist
    pricelist_remove(request.POST)

    country_states = []
    distributor_state_ids = []

    if distributor.country:
        country_states = distributor.country.country_states.all()
        distributor_state_ids = distributor.states.all().values_list('id', flat=True)


    countries = Country.objects.all().order_by('title')

    form = DistributorAddSaveForm(request.POST, instance=distributor)

    if form.is_valid():
        distributor = form.save()
        distributor_country = distributor.country
        distributor_details = distributor.details

        saved = True

    else:
        print form.errors

    # print distributor.country.id

    return {
        'distributor': distributor,
        'distributor_country': distributor_country,
        'distributor_details': distributor_details,
        'countries': countries,
        'country_states': country_states,
        'distributor_state_ids': distributor_state_ids,
        'saved': saved,
        'is_edit': True
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/brand/brand_add.html')
@get_brands
@get_countries
def brand_add_view(request):

    countries = Country.objects.all().order_by('title')
    distributors = Distributor.objects.all().order_by('title')

    return {
        'countries': countries,
        'distributors': distributors
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/brand/brand_list.html')
@get_brands
@get_countries
def brand_list_view(request):

    # user_list = User.objects.all()

    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/brand/brand_profile.html')
@get_brands
@get_countries
def brand_profile_view(request, brand_id):

    countries = Country.objects.all().order_by('title')
    brand = Brand.objects.get(id=brand_id)
    countries_attached_id = brand.countries.all().values_list('id', flat=True)

    distributors = Distributor.objects.all().order_by('title')
    distributors_attached_id = brand.brand_distributors.all().values_list('id', flat=True)

    return {
        'brand': brand,
        'countries': countries,
        'countries_attached_id': countries_attached_id,
        'distributors': distributors,
        'distributors_attached_id': distributors_attached_id
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/range/range_add.html')
@get_brands
@get_countries
def range_add_view(request, brand_id):

    # countries = Country.objects.all().order_by('title')
    # brand = Brand.objects.get(id=brand_id)
    # countries_attached_id = brand.countries.all().values_list('id', flat=True)

    return {
        'brand_id': brand_id
        # 'countries': countries,
        # 'countries_attached_id': countries_attached_id,
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/range/range_profile.html')
@get_brands
@get_countries
def range_profile_view(request, range_id):

    range = Range.objects.get(id=range_id)

    return {
        'range': range
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/sku/sku_add.html')
@get_brands
@get_countries
def sku_add_view(request, brand_id):

    ranges = Range.objects.filter(brand_id=brand_id).order_by('title')

    return {
        'ranges': ranges
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/sku/sku_profile.html')
@get_brands
@get_countries
def sku_profile_view(request, sku_id):

    sku = SKU.objects.get(id=sku_id)
    ranges = Range.objects.all()

    return {
        'sku': sku,
        'ranges': ranges
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/asset/asset_add.html')
@get_brands
@get_countries
def asset_add_view(request):

    assets = Asset.objects.all().order_by('-date_created')[:20]
    # assets = Asset.objects.none()
    assets_info = []
    parent_title = ''

    for asset in assets:

        try:
            if asset.brand:
                parent_title = asset.brand.title
            elif asset.range:
                parent_title = asset.range.title
            elif asset.sku:
                parent_title = asset.sku.title

            assets_info.append({
                'name': os.path.basename(asset.file.name),
                'size': asset.file.size if asset.file else '0',

                'url': asset.file.url if asset.file else '',
                'thumbnailUrl': asset.file_thumbnail.url if asset.file_thumbnail else '',

                'date_created_str': asset.date_created_str,

                'deleteUrl': reverse('jfu_delete', kwargs={'pk': asset.pk}),
                'deleteType': 'POST',

                'parent_title': parent_title
            })

        except:
            pass

    brands = Brand.objects.all().order_by('title')

    return {
        'assets': assets_info,
        # 'brands_info': brands_info
        'brands': brands
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/asset/asset_list.html')
@get_brands
@get_countries
def asset_list_view(request):
    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/slide/slide_add.html')
@get_brands
@get_countries
def slide_add_view(request):

    # layouts = Layout.objects.all().order_by('title')
    # languages = Language.objects.all().order_by('title')
    brands = Brand.objects.all().order_by('title')
    # ranges = Range.objects.all().order_by('title')
    # skus = SKU.objects.all().order_by('title')

    return {
        # 'layouts': layouts,
        # 'languages': languages,
        'brands': brands,
        # 'ranges': ranges,
        # 'skus': skus
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/slide/slide_list.html')
@get_brands
@get_countries
def slide_list_view(request):

    # slide_list = Slide.objects.all()

    return {
        # 'slide_list': slide_list
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/slide/slide_profile.html')
@get_brands
@get_countries
def slide_profile_view(request, slide_id):

    slide = get_object_or_None(Slide, id=slide_id)
    brands = Brand.objects.all().order_by('title')

    presentations_involved_names = []
    presentations_involved = slide.slide_presentations.all().order_by('title').values('id', 'title')
    for presentation_involved in presentations_involved:
        presentations_involved_names.append("%s. %s" % (presentation_involved['id'], presentation_involved['title'], ))

    return {
        'slide': slide,
        'brands': brands,
        'presentations_involved_names': presentations_involved_names
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/presentation/presentation_add.html')
@get_brands
@get_countries
def presentation_add_view(request):

    brands = Brand.objects.all().order_by('title')
    # ranges = Range.objects.all().order_by('title')
    # skus = SKU.objects.all().order_by('title')
    # states = State.objects.exclude(price_list__isnull=True).order_by('title')
    # distributors = Distributor.objects.all().order_by('title')
    countries = Country.objects.all().order_by('title')


    # assets_info = []
    #
    # for asset in assets:
    #
    #     assets_info.append({
    #         'name': os.path.basename(asset.file.name),
    #         'size': asset.file.size,
    #
    #         'url': asset.file.url,
    #         'thumbnailUrl': asset.file_thumbnail.url,
    #
    #         'date_created_str': asset.date_created_str,
    #
    #         'deleteUrl': reverse('jfu_delete', kwargs={'pk': asset.pk}),
    #         'deleteType': 'POST',
    #     })

    return {
        'brands': brands,
        # 'ranges': ranges,
        # 'skus': skus,
        # 'states': states,
        # 'distributors': distributors,
        'countries': countries
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/presentation/presentation_profile.html')
@get_brands
@get_countries
def presentation_profile_view(request, presentation_id):

    brands = Brand.objects.all().order_by('title')
    # distributors = Distributor.objects.all().order_by('title')
    states = State.objects.exclude(price_list__isnull=True).order_by('title')
    countries = Country.objects.all().order_by('title')

    presentation = get_object_or_None(Presentation, id=presentation_id)
    already_saved_distributors_ids = presentation.distributors.all().values_list('id', flat=True)
    slides_relations = presentation.slides.through.objects.filter(presentation=presentation).order_by('id')
    already_saved_countries_ids = presentation.countries.all().values_list('id', flat=True)

    distributors = Distributor.objects.all().order_by('title')
    selected_countries = presentation.countries.all()
    if selected_countries:
        distributors = distributors.filter(country__in=selected_countries).distinct()

    slides = []

    for slides_relation in slides_relations:
        slides.append({
            'id': slides_relation.slide_id,
            'thumbnail_url': slides_relation.slide.file_thumbnail.url,
            'title': slides_relation.slide.title,
            'title_with_id': slides_relation.slide
        })
    # ranges = Range.objects.all()
    # skus = SKU.objects.all()

    # assets_info = []
    #
    # for asset in assets:
    #
    #     assets_info.append({
    #         'name': os.path.basename(asset.file.name),
    #         'size': asset.file.size,
    #
    #         'url': asset.file.url,
    #         'thumbnailUrl': asset.file_thumbnail.url,
    #
    #         'date_created_str': asset.date_created_str,
    #
    #         'deleteUrl': reverse('jfu_delete', kwargs={'pk': asset.pk}),
    #         'deleteType': 'POST',
    #     })
    # print slides
    return {
        'brands': brands,
        'presentation': presentation,
        'slides': slides,
        'states': states,
        'distributors': distributors,
        'already_saved_distributors_ids': already_saved_distributors_ids,
        'already_saved_countries_ids': already_saved_countries_ids,
        'countries': countries
    #     'ranges': ranges,
    #     'skus': skus
    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/presentation/presentation_list.html')
@get_brands
@get_countries
def presentation_list_view(request):

    # slide_list = Slide.objects.all()

    return {
        # 'slide_list': slide_list
    }


def presentation_generate_pdf_view(request, presentation_id):

    return generate_pdf(Presentation.objects.get(id=presentation_id))


@csrf_exempt
@login_required()
@render_to('test.html')
@get_brands
@get_countries
def test(request):

    return {

    }


@csrf_exempt
@login_required()
@render_to('web-admin/pages/state/state_add.html')
@get_brands
@get_countries
def state_add_view(request, country_id):

    form = StateAddViewForm(request.POST, request.FILES)

    print form

    response = {
        'country_id': country_id,
        'form': form
    }

    if request.method == 'POST':

        if form.is_valid():
            form.save()

            response['saved'] = True

    # else:

    # countries = Country.objects.all().order_by('title')
    # brand = Brand.objects.get(id=brand_id)
    # countries_attached_id = brand.countries.all().values_list('id', flat=True)
    print form
    return response


@csrf_exempt
@login_required()
@render_to('web-admin/pages/state/state_profile.html')
@get_brands
@get_countries
def state_profile_view(request, state_id):

    state = get_object_or_None(State, id=state_id)

    form = StateAddViewForm(request.POST, request.FILES, instance=state)

    response = {
        'state': state,
        'form': form
    }

    if request.method == 'POST':

        if form.is_valid():
            form.save()

            response['saved'] = True

            response['form'] = StateAddViewForm(instance=state)
        else:
            print form.errors

    # else:

    # countries = Country.objects.all().order_by('title')
    # brand = Brand.objects.get(id=brand_id)
    # countries_attached_id = brand.countries.all().values_list('id', flat=True)

    return response