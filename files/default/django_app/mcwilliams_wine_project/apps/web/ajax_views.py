import os
import json
import re
import urllib2
import datetime
import pytz

from base64 import b64decode, urlsafe_b64decode
from StringIO import StringIO

from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_POST
from django.core.files.base import ContentFile
from django.core.files import File
from django.db.models import Q

from mcwilliams_wine_project.apps.web.functions import render_to, ajax_request, gen_presentation_archive, \
    gen_presentation_archive_s3, get_copy_name, encode_url, get_asset_future_path
from mcwilliams_wine_project.apps.general_functions.functions import get_object_or_None
from mcwilliams_wine_project.apps.db.models import User, Country, Distributor, Brand, Range, SKU, Asset, Slide, \
    SlideAsset, TempPreviewImage, Presentation, SlideElement, State
from mcwilliams_wine_project.apps.web.forms import UserAddSaveForm, UserIdForm, CountryAddSaveForm, \
    DistributorAddSaveForm, BrandAddSaveForm, RangeAddSaveForm, SKUAddSaveForm, SlideAddSaveForm, PresentationAddSaveForm

from jfu.http import upload_receive, UploadResponse, JFUResponse


@csrf_exempt
@ajax_request
def user_add_save(request):

    user_id = request.POST.get('user_id')

    user = None

    if user_id:
        user = get_object_or_None(User, id=user_id)
        old_password = user.password

    form = UserAddSaveForm(request.POST, instance=user)

    if form.is_valid():

        user_instance = form.save(commit=False)

        password = form.cleaned_data['password']

        #
        if not user or (user and password):
            user_instance.set_password(password)

        else:
            user_instance.password = old_password

        user_instance.save()

        return {'success': True}

    else:

        return {'success': False, 'errors': 'Email is already taken. Please choose another one.', 'errors_dict': form.errors}


@csrf_exempt
@ajax_request
def user_delete(request):

    form = UserIdForm(request.POST)

    if form.is_valid():

        user_id = form.cleaned_data['user_id']

        user = get_object_or_None(User, id=user_id)
        if not user:
            return {'success': False, 'errors': 'User does not exist'}

        if user == request.user:
            return {'success': False, 'errors': 'You cannot delete yourself'}

        user.delete()

        return {'success': True}

    else:
        return {'success': False, 'errors': form.errors}


@csrf_exempt
@ajax_request
def user_list_table(request):

    user_list = User.objects.all()

    user_info_list = []
    for user in user_list:

        user_info_list.append({
            'surname': user.surname,
            'first_name': user.first_name,
            'distributor': user.distributor.title if user.distributor else '',
            'phone': user.phone,
            'email': user.email,
            'role': user.get_role_display(),
            'role_id': user.role,
            'id': user.id,
            'is_deletable': user.id != request.user.id
        })

    return {'success': True, 'aaData': user_info_list}


@csrf_exempt
@ajax_request
def user_check_email_is_free(request):

    email = request.GET.get('email')
    users = User.objects.filter(email__iexact=email)

    if users.exists():
        return {'success': False, 'error': 'Email is already busy'}

    return {'success': True}


@csrf_exempt
@ajax_request
def country_add_save(request):

    country_id = request.POST.get('country_id')
    country = None

    if country_id:
        country = get_object_or_None(Country, id=country_id)

    form = CountryAddSaveForm(request.POST, instance=country)

    if form.is_valid():

        form.save()

        return {'success': True}

    else:

        return {'success': False, 'errors': 'Country title is already taken. Please choose another one.', 'errors_dict': form.errors}


@csrf_exempt
@ajax_request
def country_list_table(request):

    country_list = Country.objects.all()

    country_info_list = []
    for country in country_list:

        country_info_list.append({
            'title': country.title,
            'features': country.features[:100],
            'id': country.id
        })

    return {'success': True, 'aaData': country_info_list}


@csrf_exempt
@ajax_request
def country_delete(request):

    country_id = request.POST.get('country_id')

    country = get_object_or_None(Country, id=country_id)
    if not country:
        return {'success': False, 'errors': 'Country does not exist'}

    country.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def distributor_add_save(request):

    distributor_id = request.POST.get('distributor_id')
    distributor = None

    if distributor_id:
        distributor = get_object_or_None(Distributor, id=distributor_id)

    form = DistributorAddSaveForm(request.POST, instance=distributor)

    if form.is_valid():

        form.save()

        return {'success': True}

    else:

        return {'success': False, 'errors': 'Distributor title is already taken. Please choose another one.', 'errors_dict': form.errors}


@csrf_exempt
@ajax_request
def distributor_list_table(request):

    distributor_list = Distributor.objects.all().order_by('title')

    distributor_info_list = []
    for distributor in distributor_list:

        states = []
        for state in distributor.states.all().order_by('title'):
            # states_str = ', '.join(state.title)
            states.append(state.title)

        distributor_info_list.append({
            'title': distributor.title,
            'country': distributor.country.title if distributor.country else '',
            'states':  ', '.join(states),
            'details': distributor.details[:100],
            'id': distributor.id
        })

    return {'success': True, 'aaData': distributor_info_list}


@csrf_exempt
@ajax_request
def distributor_delete(request):

    distributor_id = request.POST.get('distributor_id')

    distributor = get_object_or_None(Distributor, id=distributor_id)
    if not distributor:
        return {'success': False, 'errors': 'Distributor does not exist'}

    distributor.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def brand_add_save(request):

    brand_id = request.POST.get('brand_id')
    brand = None

    if brand_id:
        brand = get_object_or_None(Brand, id=brand_id)

    form = BrandAddSaveForm(request.POST, instance=brand)

    if form.is_valid():

        brand_obj = form.save()
        brand_obj.brand_distributors = request.POST.getlist('distributors')

        return {'success': True}

    else:

        return {'success': False, 'errors': 'Brand title is already taken. Please choose another one.', 'errors_dict': form.errors}


@csrf_exempt
@ajax_request
def brand_list_table(request):

    brand_list = Brand.objects.all().order_by('title')

    brand_info_list = []
    for brand in brand_list:

        brand_info_list.append({
            'title': brand.title,
            'id': brand.id
        })

    return {'success': True, 'aaData': brand_info_list}


@csrf_exempt
@ajax_request
def brand_delete(request):

    brand_id = request.POST.get('brand_id')

    brand = get_object_or_None(Brand, id=brand_id)
    if not brand:
        return {'success': False, 'errors': 'Brand does not exist'}

    brand.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def range_add_save(request):

    range_id = request.POST.get('range_id')
    range = None

    if range_id:
        range = get_object_or_None(Range, id=range_id)

    form = RangeAddSaveForm(request.POST, instance=range)

    brand = get_object_or_None(Brand, id=request.POST.get('brand'))

    if form.is_valid():

        form.save()

        return {'success': True}

    else:

        return {
            'success': False,
            'errors': 'Range title within "%s" brand is already taken. Please choose another one.' % (brand.title),
            'errors_dict': form.errors
        }


@csrf_exempt
@ajax_request
def range_delete(request):

    range_id = request.POST.get('range_id')

    range = get_object_or_None(Range, id=range_id)
    if not range:
        return {'success': False, 'errors': 'Range does not exist'}

    range.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def range_list_table(request):

    brand_id = request.GET.get('brand_id')

    range_list = Range.objects.all().order_by('title')

    if brand_id:
        range_list = range_list.filter(brand_id=brand_id)

    range_info_list = []
    for range in range_list:

        range_info_list.append({
            'title': range.title,
            'id': range.id
        })

    return {'success': True, 'aaData': range_info_list}


@csrf_exempt
@ajax_request
def sku_add_save(request):

    sku_id = request.POST.get('sku_id')
    sku = None

    if sku_id:
        sku = get_object_or_None(SKU, id=sku_id)

    form = SKUAddSaveForm(request.POST, instance=sku)

    if form.is_valid():

        form.save()

        return {'success': True}

    else:

        return {
            'success': False,
            'errors': 'SKU title is already taken. Please choose another one.',
            'errors_dict': form.errors
        }


@csrf_exempt
@ajax_request
def sku_list_table(request):

    brand_id = request.GET.get('brand_id')

    sku_list = SKU.objects.all().order_by('title')

    if brand_id:
        sku_list = sku_list.filter(range__brand_id=brand_id)

    sku_info_list = []
    for sku in sku_list:

        sku_info_list.append({
            'title': sku.title,
            'range': sku.range.title,
            'sku_num': sku.sku_num,
            'vintage_num': sku.vintage_num,
            'id': sku.id
        })

    return {'success': True, 'aaData': sku_info_list}


@csrf_exempt
@ajax_request
def sku_delete(request):

    sku_id = request.POST.get('sku_id')

    sku = get_object_or_None(SKU, id=sku_id)
    if not sku:
        return {'success': False, 'errors': 'SKU does not exist'}

    sku.delete()

    return {'success': True}


@ajax_request
def asset_will_override(request):

    subtype = request.GET.get('subtype')
    type = request.GET.get('type')
    id = request.GET.get('id')
    filename = request.GET.get('filename')

    instance = Asset(
        file=file, subtype=subtype
    )

    if type == 'brand':
        instance.brand = Brand.objects.get(id=id)

    elif type == 'range':
        instance.range = Range.objects.get(id=id)

    elif type == 'sku':
        instance.sku = SKU.objects.get(id=id)

    asset_future_path = get_asset_future_path(instance, filename)

    print 'old-asset'
    print Asset.objects.filter(file__iexact=asset_future_path)

    return {'success': True, 'will_override': Asset.objects.filter(file__iexact=asset_future_path).exists()}


@require_POST
def asset_add_save(request):

    # The assumption here is that jQuery File Upload
    # has been configured to send files one at a time.
    # If multiple files can be uploaded simulatenously,
    # 'file' may be a list of files.
    file = upload_receive(request)

    # return request.FILES['files[]'] if request.FILES else None

    subtype = request.POST.get('subtype')
    type = request.POST.get('type')
    id = request.POST.get('id')

    instance = Asset(
        file=file, subtype=subtype
    )

    if type == 'brand':
        instance.brand_id = id

    elif type == 'range':
        instance.range_id = id

    elif type == 'sku':
        instance.sku_id = id

    instance.save()

    #as we need object with thumbnail created
    asset_after_save = Asset.objects.get(id=instance.id)

    # remove old assets with the same file path/name as the current new one.
    Asset.objects.filter(file__iexact=asset_after_save.file.name).exclude(id=asset_after_save.id).delete()

    file_dict = {
        'name': os.path.basename(instance.file.path) if not settings.USE_S3 else instance.file.name.split('/')[-1],
        'size': file.size,

        'url': instance.file.url,
        'thumbnailUrl': asset_after_save.file_thumbnail.url,

        'deleteUrl': reverse('jfu_delete', kwargs={'pk': instance.pk}),
        'deleteType': 'POST',
    }

    return UploadResponse(request, file_dict)


@require_POST
def upload_delete(request, pk):
    success = True
    try:
        instance = Asset.objects.get(pk=pk)
        instance.delete()
    except Asset.DoesNotExist:
        success = False

    return JFUResponse(request, success)


@csrf_exempt
@ajax_request
def asset_list_table(request):

    asset_list = Asset.objects.all()

    asset_info_list = []
    for asset in asset_list:

        asset_info_list.append({
            'title': asset.file_name,
            'location': asset.location,
            'date_created': asset.date_created_str,
            'thumbnail': asset.thumbnail_url,
            'id': asset.id,
            'file': asset.file.url if asset.file else ''
        })

    return {'success': True, 'aaData': asset_info_list}


@csrf_exempt
@ajax_request
def asset_delete(request):

    asset_id = request.POST.get('asset_id')

    asset = get_object_or_None(Asset, id=asset_id)
    if not asset:
        return {'success': False, 'errors': 'Asset does not exist'}

    slide_elements = SlideElement.objects.filter(
        Q(file__icontains=asset.file.url) |
        Q(action__icontains=asset.file.url)
    )

    has_dependencies = slide_elements.exists()

    return {'success': True, 'has_dependencies': has_dependencies}


@csrf_exempt
@ajax_request
def asset_has_dependencies(request):

    asset_id = request.GET.get('asset_id')

    asset = get_object_or_None(Asset, id=asset_id)
    if not asset:
        return {'success': False, 'errors': 'Asset does not exist'}

    slides_involved = SlideElement.objects.filter(
        Q(file__icontains=asset.file.url) |
        Q(action__icontains=asset.file.url)
    ).values_list('slide__title', flat=True).order_by('slide__title')

    has_dependencies = len(slides_involved) != 0

    return {'success': True, 'has_dependencies': has_dependencies, 'slides_involved': ', '.join(slides_involved)}


@csrf_exempt
@ajax_request
def slide_add_save(request):

    slide_id = request.POST.get('slide_id')
    slide_elements = request.POST.get('slide_elements')

    slide_elements = json.loads(slide_elements)
    slide_thumbnail_base64 = request.POST.get('slide_thumbnail')

    slide_thumbnail_clear_base64 = re.search(r'base64,(.*)', slide_thumbnail_base64).group(1)
    image_data = b64decode(slide_thumbnail_clear_base64)

    slide = None

    if slide_id:
        slide = get_object_or_None(Slide, id=slide_id)

    form = SlideAddSaveForm(request.POST, instance=slide)

    if form.is_valid():

        slide = form.save(commit=False)
        slide.user = request.user
        slide.file = ContentFile(image_data, 'whatup.jpg')
        slide.save()

        SlideElement.objects.filter(slide=slide).delete()

        for slide_element in slide_elements:

            new_slide_element = SlideElement.objects.create(
                slide=slide,
                layout_element_id=slide_element['element_id'],
                file=slide_element.get('file_url', ''),
                content=slide_element['content'],
                x=slide_element['x'],
                y=slide_element['y'],
                width=slide_element['width'],
                height=slide_element['height'],
                z_index=slide_element['z_index'],
                action=slide_element['action'],
                resizable=slide_element['resizable'],
                movable=slide_element['movable'],
                line_height=slide_element.get('line_height', '100%'),
                text_align=slide_element.get('text_align', SlideElement.TEXT_ALIGN_LEFT),
                vertical_align=slide_element.get('vertical_align', SlideElement.VERTICAL_ALIGN_TOP),
                font_id=slide_element.get('font_id'),
                font_size=slide_element.get('font_size', '20'),
                font_color=slide_element.get('font_color', '#000000'),
                content_type=slide_element.get('content_type', 1),
                label=slide_element.get('label', ''),
                property=slide_element.get('property', ''),
            )

            # is background video
            if slide_element['content_type'] == 3:

                old_name_arr = slide_element.get('file_url', '').split('/')
                slide.file.name = '/'.join(old_name_arr[3:])
                 # = self.file.name
                # self.file.name =

                slide.file_thumbnail = ContentFile(image_data, 'whatup.jpg')
                slide.save()

        return {'success': True, 'slide_id': slide.id}

    else:

        return {
            'success': False,
            'errors': form.errors.as_text(),
            'errors_dict': form.errors
        }


@csrf_exempt
@ajax_request
def slide_list_table(request):

    slide_list = Slide.objects.all().order_by('title')

    slide_info_list = []
    for slide in slide_list:

        slide_info_list.append({
            'title': slide.title,
            'brand': slide.brand.title if slide.brand else '',
            'range': slide.range.title if slide.range else '',
            'sku': slide.sku.title if slide.sku else '',
            'preview': slide.get_thumbnail(),
            'author': slide.user.first_name,
            'id': slide.id
        })

    return {'success': True, 'aaData': slide_info_list}


@csrf_exempt
@ajax_request
def slide_delete(request):

    slide_id = request.POST.get('slide_id')

    slide = get_object_or_None(Slide, id=slide_id)
    if not slide:
        return {'success': False, 'errors': 'Slide does not exist'}

    slide.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def slide_copy(request):

    slide_id = request.POST.get('slide_id')

    slide = get_object_or_None(Slide, id=slide_id)
    if not slide:
        return {'success': False, 'errors': 'Slide does not exist'}

    slide_elements = SlideElement.objects.filter(slide=slide)

    # Copy Slide
    slide.pk = None
    slide.title = get_copy_name(slide.title)
    slide.save()

    #Copy SlideElements
    for slide_element in slide_elements:
        slide_element.pk = None
        slide_element.slide = slide
        slide_element.save()

    return {'success': True}


@csrf_exempt
@ajax_request
def presentation_add_save(request):

    presentation_id = request.POST.get('presentation_id')
    distributors = request.POST.getlist('distributors')
    countries = request.POST.getlist('countries')

    presentation = None

    if presentation_id:
        presentation = get_object_or_None(Presentation, id=presentation_id)

    slides_id = request.POST.get('slides')

    form = PresentationAddSaveForm(request.POST, instance=presentation)

    if form.is_valid():

        thumbnail_url = form.cleaned_data['thumbnail_url']

        presentation = form.save(commit=False)
        presentation.user = request.user
        presentation.save()

        if distributors:
            presentation.distributors = distributors
        elif countries and not distributors:
            presentation.distributors = Distributor.objects.filter(id__in=distributors).values_list('id', flat=True)
        else:
            presentation.distributors = Distributor.objects.all().values_list('id', flat=True)

        presentation.countries = countries

        if slides_id:
            presentation.slides.clear()
            for slide in slides_id.split(','):
                presentation.slides.add(slide)

        if thumbnail_url:
            s = StringIO()
            s.write(urllib2.urlopen(encode_url(thumbnail_url)).read())
            s.size = s.tell()

            presentation.thumbnail.save('test.jpg', File(s), save=True)

        if settings.USE_S3:
            archive_temp_path = gen_presentation_archive_s3(presentation)
        else:
            archive_temp_path = gen_presentation_archive(presentation)

        reopen = open(archive_temp_path, "rb")
        django_file = File(reopen)

        presentation.archive.save('.zip', django_file, save=True)

        os.unlink(archive_temp_path)

        return {'success': True, 'presentation_id': presentation.id}

    else:

        return {
            'success': False,
            'errors': 'Error happened',
            'errors_dict': form.errors
        }


@csrf_exempt
@ajax_request
def presentation_list_table(request):

    presentation_list = Presentation.objects.all().order_by('title')

    presentation_info_list = []
    for presentation in presentation_list:

        slides = []
        for slide_relation in presentation.slides.through.objects.filter(presentation=presentation).order_by('id'):

            slides.append(slide_relation.slide.get_thumbnail())

        presentation_info_list.append({
            'title': presentation.title,
            'description': presentation.description,
            'sku': presentation.sku.title if presentation.sku else '',
            'slides': slides,
            'author': presentation.user.first_name,
            'id': presentation.id
        })

    return {'success': True, 'aaData': presentation_info_list}


@csrf_exempt
@ajax_request
def presentation_delete(request):

    presentation_id = request.POST.get('presentation_id')

    presentation = get_object_or_None(Presentation, id=presentation_id)
    if not presentation:
        return {'success': False, 'errors': 'Presentation does not exist'}

    presentation.delete()

    return {'success': True}


@ajax_request
def get_zip(request):
    gen_presentation_archive(Presentation.objects.all()[0])
    return {}


@csrf_exempt
@ajax_request
def state_list_table(request):

    country_id = request.GET.get('country_id')

    state_list = State.objects.filter(country_id=country_id).order_by('title')

    state_info_list = []
    for state in state_list:

        state_info_list.append({
            'title': state.title,
            # 'has_price_list': 'Yes' if state.price_list else 'No',
            'id': state.id
        })

    return {'success': True, 'aaData': state_info_list}


@csrf_exempt
@ajax_request
def state_delete(request):

    state_id = request.POST.get('state_id')

    state = get_object_or_None(State, id=state_id)
    if not state:
        return {'success': False, 'errors': 'State does not exist'}

    state.delete()

    return {'success': True}


@csrf_exempt
@ajax_request
def state_add_save(request):

    title = request.POST.get('title')
    country_id = request.POST.get('country_id')

    State.objects.create(
        title=title,
        country_id=country_id
    )

    return {'success': True}