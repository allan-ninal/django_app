from django.contrib.gis import admin

from mcwilliams_wine_project.apps.db.models import User, Distributor, Country, Brand, Range, SKU, Presentation, \
    Slide, Language, Asset,  AppLogging, AppVersion, LayoutElement, Layout, \
    TempPreviewImage, SlideAsset, SlideElement, Font, State, Pricelist


class  customLayout(admin.ModelAdmin):
    filter_horizontal = ['elements']

admin.site.register(User)
admin.site.register(Distributor)
admin.site.register(Country)
admin.site.register(Brand)
admin.site.register(Range)
admin.site.register(SKU)
admin.site.register(Presentation)
admin.site.register(Slide)
admin.site.register(Language)
admin.site.register(Asset)
admin.site.register(AppLogging)
admin.site.register(AppVersion)
admin.site.register(LayoutElement)
admin.site.register(Layout, customLayout)
admin.site.register(TempPreviewImage)
# admin.site.register(SlideAsset)
admin.site.register(SlideElement)
admin.site.register(Font)
admin.site.register(State)
admin.site.register(Pricelist)
