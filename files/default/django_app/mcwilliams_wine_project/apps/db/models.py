import os
import hashlib
import datetime

from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.files import get_thumbnailer

from .managers import UserManager


def set_file_name(instance, filename):
    class_name = instance.__class__.__name__.lower()
    class_name_with_s = class_name + 's'
    file_ext = filename.split('.')[-1]
    file_name = filename.split('/')[-1]
    entities_to_save_their_name = ['asset', 'state']

    try:
        os.mkdir(os.path.join(settings.MEDIA_ROOT, class_name))
    except OSError:
        pass

    salt = hashlib.md5(str(datetime.datetime.now())).hexdigest()[:30]

    if class_name in entities_to_save_their_name:
        return os.path.join(class_name_with_s, filename)

    return os.path.join(class_name_with_s, salt + '.' + file_ext)


class User(AbstractBaseUser, PermissionsMixin):

    MASTER_ADMIN = 1
    CONTENT_MANAGER = 2
    END_USER = 3

    ROLE_CHOICES = (
        (MASTER_ADMIN, 'Master Admin'),
        (CONTENT_MANAGER, 'Content Manager'),
        (END_USER, 'End User'),
    )

    ROLE_NO_ADMIN_CHOICES = (
        (CONTENT_MANAGER, 'Content Manager'),
        (END_USER, 'End User'),
    )

    email = models.EmailField(max_length=200, unique=True)
    surname = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=100, blank=True, null=True, default='')
    # date_of_birth = models.DateTimeField(blank=True, null=True)
    details = models.TextField(blank=True, null=True, default='')

    role = models.SmallIntegerField(choices=ROLE_CHOICES, default=CONTENT_MANAGER)
    is_active = models.BooleanField(default=True)

    date_registered = models.DateTimeField(auto_now_add=True)

    distributor = models.ForeignKey('Distributor', null=True, blank=True, related_name='distributor_users', on_delete=models.SET_NULL)
    country = models.ForeignKey('Country', null=True, blank=True, related_name='country_users', on_delete=models.SET_NULL)
    state = models.ForeignKey('State', null=True, blank=True, related_name='state_users', on_delete=models.SET_NULL)
    # regions = db.ManyToManyField('Region', null=True, blank=True, related_name='region_users')

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return '%i. %s' % (self.id, self.email)

    def has_perms(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return True

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.surname)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def save(self, *args, **kwargs):

        import datetime

        self.last_login = datetime.datetime.utcnow()

        super(User, self).save(*args, **kwargs)


class Distributor(models.Model):

    title = models.CharField(max_length=200)
    details = models.TextField(null=True, blank=True, default='')
    country = models.ForeignKey('Country', null=True, blank=True, related_name='country_distributors')
    states = models.ManyToManyField('State', null=True, blank=True, related_name='state_distributors')
    brands = models.ManyToManyField('Brand', null=True, blank=True, related_name='brand_distributors')

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)


class Country(models.Model):

    title = models.CharField(max_length=200)
    features = models.TextField(null=True, blank=True, default='')

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)


class State(models.Model):

    title = models.CharField(max_length=255)
    country = models.ForeignKey('Country', related_name='country_states')
    price_list = models.FileField(upload_to=set_file_name, max_length=255, blank=True, null=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import gen_presentation_archive_s3
        from django.core.files import File
        do_compare = False
        if self.pk:
            do_compare = True
            price_list_before = State.objects.get(id=self.id).price_list
            price_list_after = self.price_list

        super(State, self).save(*args, **kwargs)
        if do_compare:

            if self.state_presentations and price_list_after != price_list_before:
                slideshows = self.state_presentations.all()
                for slideshow in slideshows:
                    archive_temp_path = gen_presentation_archive_s3(slideshow)

                    reopen = open(archive_temp_path, "rb")
                    django_file = File(reopen)
                    slideshow.archive.save('.zip', django_file, save=True)
                    os.unlink(archive_temp_path)


class Brand(models.Model):

    title = models.CharField(max_length=200)
    date_created = models.DateTimeField(auto_now_add=True)
    countries = models.ManyToManyField('Country', null=True, blank=True, related_name='country_brands')

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    @property
    def safe_title(self):

        title = self.title.replace(' ', '_')

        return "".join(symbol for symbol in title if symbol.isalnum() or symbol == '_')

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import create_home_dir

        super(Brand, self).save(*args, **kwargs)


class Range(models.Model):

    title = models.CharField(max_length=200)
    brand = models.ForeignKey('Brand', related_name='brand_ranges')

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    @property
    def safe_title(self):

        title = self.title.replace(' ', '_')

        return "".join(symbol for symbol in title if symbol.isalnum() or symbol == '_')

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import create_home_dir

        super(Range, self).save(*args, **kwargs)

        # create_home_dir(self, [self.brand])


class SKU(models.Model):

    # from mcwilliams_wine_project.apps.web.functions import create_home_dir

    title = models.CharField(max_length=200)
    range = models.ForeignKey('Range', related_name='range_sku')
    sku_num = models.CharField(max_length=10, null=True, blank=True)
    vintage_num = models.CharField(max_length=4, null=True, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    @property
    def safe_title(self):

        title = self.title.replace(' ', '_')

        return "".join(symbol for symbol in title if symbol.isalnum() or symbol == '_')

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import create_home_dir

        super(SKU, self).save(*args, **kwargs)

        # create_home_dir(self, [self.range.brand, self.range])


class Presentation(models.Model):

    title = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True, default='')

    user = models.ForeignKey('User', related_name='user_presentations')
    sku = models.ForeignKey('SKU', related_name='sku_presentations', null=True, blank=True)
    slides = models.ManyToManyField('Slide', related_name='slide_presentations')
    state = models.ForeignKey('State', related_name='state_presentations', null=True, blank=True)
    country = models.ForeignKey('Country', related_name='country_presentations', null=True, blank=True)
    countries = models.ManyToManyField('Country', related_name='country_presentations_m2m', null=True, blank=True)
    distributors = models.ManyToManyField('Distributor', related_name='distributor_presentations', null=True, blank=True)

    archive = models.FileField(upload_to=set_file_name, default='', blank=True, null=True, max_length=255)
    thumbnail = models.FileField(upload_to=set_file_name, default='', blank=True, null=True, max_length=255)

    date_created = models.DateTimeField(auto_now_add=True)
    date_edited = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    def delete(self, using=None):

        if self.archive:
            self.archive.delete(save=False)

        super(Presentation, self).delete()

    def get_thumbnail_name(self):
        img_name = ''

        if self.thumbnail:

            img_name = self.thumbnail.name.split('/')[-1]

        return img_name


class Slide(models.Model):

    title = models.CharField(max_length=200)

    user = models.ForeignKey('User', related_name='user_slides')

    presentation = models.ForeignKey('Presentation', null=True, blank=True, related_name='presentation_slides')
    language = models.ForeignKey('Language', null=True, blank=True, related_name='language_slides')

    brand = models.ForeignKey('Brand', null=True, blank=True, related_name='brand_slides')
    range = models.ForeignKey('Range', null=True, blank=True, related_name='range_slides')
    sku = models.ForeignKey('SKU', null=True, blank=True, related_name='sku_slides')

    layout = models.ForeignKey('Layout', null=True, blank=True, related_name='layout_slides')

    file = models.FileField(upload_to=set_file_name, default='', max_length=255)
    file_thumbnail = models.FileField(upload_to=set_file_name, default='', max_length=255)

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    @property
    def is_video(self):
        return self.file.name.split('.')[-1].lower() not in settings.IMG_EXTS

    def get_thumbnail(self):

        thumbnail_url = "%s%s%s%s%s" % (settings.STATIC_URL, 'images/', 'placeholders/', 'slide/', 'placeholder.png')

        if self.file_thumbnail:
            thumbnail_url = self.file_thumbnail.url

        elif self.file:
            thumbnail_url = self.file.url

        return thumbnail_url

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import gen_presentation_archive_s3
        from django.core.files import File

        try:
            this = Slide.objects.get(id=self.id)
            if this.file != self.file:
                this.file.delete(save=False)

            if this.file_thumbnail != self.file_thumbnail:
                this.file_thumbnail.delete(save=False)
        except:
            pass

        super(Slide, self).save(*args, **kwargs)

        try:
            Slide.objects.filter(id=self.id).update(file_thumbnail=get_thumbnailer(self.file)['slide_thumbnail'])
        except:
            pass

        if self.slide_presentations:
            slideshows = self.slide_presentations.all()
            for slideshow in slideshows:

                try:
                    archive_temp_path = gen_presentation_archive_s3(slideshow)

                    reopen = open(archive_temp_path, "rb")
                    django_file = File(reopen)
                    slideshow.archive.save('.zip', django_file, save=True)
                    os.unlink(archive_temp_path)
                except:
                    pass


    def delete(self, using=None):

        # try:
        #     if self.file:
        #         self.file.delete(save=False)
        #
        #     if self.file_thumbnail:
        #         self.file_thumbnail.delete(save=False)
        # except:
        #     pass

        super(Slide, self).delete()

    # class Meta:
    #     abstract = True


class SlideAsset(models.Model):

    slide = models.ForeignKey('Slide')
    asset = models.ForeignKey('Asset')
    type = models.ForeignKey('ElementType', blank=True, null=True)

    file = models.FilePathField(path=settings.ASSETS_ROOT, recursive=True, max_length=255)
    content = models.TextField(blank=True, null=True, default='')

    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)

    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)

    z_index = models.IntegerField(default=-1)

    def __unicode__(self):
        return '%i. Of slide: %s' % (self.id, self.slide.title)

    # class Meta:
    #     abstract = True


class Layout(models.Model):

    title = models.CharField(max_length=200)
    thumbnail = models.ImageField(upload_to='layouts/', max_length=255, null=True, blank=True, default='')
    elements = models.ManyToManyField('LayoutElement', related_name='layouts')

    brand = models.ForeignKey('Brand', null=True, blank=True, related_name='brand_layouts')

    def get_thumbnail(self):

        thumbnail_url = "%s%s%s%s%s" % (settings.STATIC_URL, 'images/', 'placeholders/', 'layout/', 'layout-placeholder.png')

        if self.thumbnail:
            thumbnail_url = self.thumbnail.url

        return thumbnail_url

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)

    # class Meta:
    #     abstract = True


class ElementType(models.Model):

    TEXT = 1
    IMAGE = 2
    VIDEO = 3
    TEXT_PLACEHOLDER = 4

    CONTENT_TYPE_CHOICES = (
        (TEXT, 'Text'),
        (IMAGE, 'Image'),
        (VIDEO, 'Video'),
        (TEXT_PLACEHOLDER, 'Text placeholder'),
    )

    NONE = 0
    PERSON_NAME = 1
    COMPANY_NAME = 2
    DATE = 3

    TEXT_PLACEHOLDER_TYPE_CHOICES = (
        (NONE, 'None'),
        (PERSON_NAME, 'Person Name'),
        (COMPANY_NAME, 'Company Name'),
        (DATE, 'Date')
    )

    title = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    content_type = models.SmallIntegerField(choices=CONTENT_TYPE_CHOICES, default=TEXT)
    z_index = models.IntegerField(default=1)

    text_placeholder_type = models.SmallIntegerField(choices=TEXT_PLACEHOLDER_TYPE_CHOICES, default=NONE)

    def __unicode__(self):
        return '%s. %s' % (self.title, self.get_content_type_display())

    # class Meta:
    #     abstract = True


class LayoutElement(models.Model):

    TEXT = 1
    IMAGE = 2
    VIDEO = 3
    BUTTON = 4
    TEXT_PLACEHOLDER = 5

    CONTENT_TYPE_CHOICES = (
        (TEXT, 'Text'),
        (IMAGE, 'Image'),
        (VIDEO, 'Video'),
        (BUTTON, 'Button'),
        (TEXT_PLACEHOLDER, 'Text placeholder')
    )

    YES = 1
    NO = 0

    BOOLEAN_CHOICES = (
        (YES, 'Yes'),
        (NO, 'No'),
    )

    NONE = 'none'
    PERSON_NAME = 'person_name'
    COMPANY_NAME = 'company_name'
    DATE = 'date'

    TEXT_PLACEHOLDER_TYPE_CHOICES = (
        (NONE, 'None'),
        (PERSON_NAME, 'Person Name'),
        (COMPANY_NAME, 'Company Name'),
        (DATE, 'Date')
    )


    TEXT_ALIGN_LEFT = 'left'
    TEXT_ALIGN_CENTER = 'center'
    TEXT_ALIGN_RIGHT = 'right'

    TEXT_ALIGN_CHOICES = (
        (TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT),
        (TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER),
        (TEXT_ALIGN_RIGHT, TEXT_ALIGN_RIGHT)
    )

    VERTICAL_ALIGN_TOP = 'top'
    VERTICAL_ALIGN_MIDDLE = 'middle'
    VERTICAL_ALIGN_BOTTOM = 'bottom'

    VERTICAL_ALIGN_CHOICES = (
        (VERTICAL_ALIGN_TOP, VERTICAL_ALIGN_TOP),
        (VERTICAL_ALIGN_MIDDLE, VERTICAL_ALIGN_MIDDLE),
        (VERTICAL_ALIGN_BOTTOM, VERTICAL_ALIGN_BOTTOM)
    )

    order_number = models.SmallIntegerField(default=1)

    label = models.CharField(max_length=255, default='')
    property = models.CharField(max_length=255, default='')

    content_type = models.SmallIntegerField(choices=CONTENT_TYPE_CHOICES, default=TEXT)

    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)

    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)

    z_index = models.IntegerField(default=1)

    resizable = models.SmallIntegerField(choices=BOOLEAN_CHOICES, default=YES)
    movable = models.SmallIntegerField(choices=BOOLEAN_CHOICES, default=YES)

    line_height = models.CharField(max_length=255, default='100%', blank=True, null=True)
    text_align = models.CharField(max_length=255, default=TEXT_ALIGN_LEFT, blank=True, null=True, choices=TEXT_ALIGN_CHOICES)
    vertical_align = models.CharField(max_length=255, default=VERTICAL_ALIGN_TOP, blank=True, null=True, choices=VERTICAL_ALIGN_CHOICES)

    font = models.ForeignKey('Font', blank=True, null=True)
    font_size = models.IntegerField(default='20', blank=True, null=True)
    font_color = models.CharField(max_length=255, blank=True, null=True, default="#000000")

    predefined_text_content = models.TextField(default='', blank=True, null=True)
    predefined_image = models.FileField(upload_to=set_file_name, max_length=255, null=True, blank=True, default='')

    text_placeholder_type = models.CharField(max_length=100,choices=TEXT_PLACEHOLDER_TYPE_CHOICES, default=PERSON_NAME)

    def __unicode__(self):
        if self.layouts.all():
            return '%i. %s. %s. %i %i. %ix%i. %s' % (
                self.id, self.label, self.get_content_type_display(), self.x, self.y, self.width, self.height, self.layouts.all()[0].title
            )
        else:
            return '%i. %s. %s. %i %i. %ix%i.' % (
                self.id, self.label, self.get_content_type_display(), self.x, self.y, self.width, self.height
            )

    def get_predefined_image_name(self):
        img_name = ''

        if self.predefined_image:

            img_name = self.predefined_image.name.split('/')[-1]

        return img_name

    # class Meta:
    #     abstract = True


class SlideElement(models.Model):

    TEXT = 1
    IMAGE = 2
    VIDEO = 3
    BUTTON = 4
    TEXT_PLACEHOLDER = 5

    CONTENT_TYPE_CHOICES = (
        (TEXT, 'Text'),
        (IMAGE, 'Image'),
        (VIDEO, 'Video'),
        (BUTTON, 'Button'),
        (TEXT_PLACEHOLDER, 'Text placeholder')
    )

    YES = 1
    NO = 0

    BOOLEAN_CHOICES = (
        (YES, 'Yes'),
        (NO, 'No'),
    )

    NONE = 'none'
    PERSON_NAME = 'person_name'
    COMPANY_NAME = 'company_name'
    DATE = 'date'

    TEXT_PLACEHOLDER_TYPE_CHOICES = (
        (NONE, 'None'),
        (PERSON_NAME, 'Person Name'),
        (COMPANY_NAME, 'Company Name'),
        (DATE, 'Date')
    )



    TEXT_ALIGN_LEFT = 'left'
    TEXT_ALIGN_CENTER = 'center'
    TEXT_ALIGN_RIGHT = 'right'

    TEXT_ALIGN_CHOICES = (
        (TEXT_ALIGN_LEFT, TEXT_ALIGN_LEFT),
        (TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER),
        (TEXT_ALIGN_RIGHT, TEXT_ALIGN_RIGHT)
    )

    VERTICAL_ALIGN_TOP = 'top'
    VERTICAL_ALIGN_MIDDLE = 'middle'
    VERTICAL_ALIGN_BOTTOM = 'bottom'

    VERTICAL_ALIGN_CHOICES = (
        (VERTICAL_ALIGN_TOP, VERTICAL_ALIGN_TOP),
        (VERTICAL_ALIGN_MIDDLE, VERTICAL_ALIGN_MIDDLE),
        (VERTICAL_ALIGN_BOTTOM, VERTICAL_ALIGN_BOTTOM)
    )

    slide = models.ForeignKey('Slide')
    layout_element = models.ForeignKey('LayoutElement')

    file = models.CharField(blank=True, null=True, max_length=255)
    content = models.TextField(blank=True, null=True, default='')

    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)

    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)

    z_index = models.IntegerField(default=1)

    resizable = models.SmallIntegerField(choices=BOOLEAN_CHOICES, default=YES)
    movable = models.SmallIntegerField(choices=BOOLEAN_CHOICES, default=YES)

    line_height = models.CharField(max_length=255, default='100%', blank=True, null=True)
    text_align = models.CharField(max_length=255, default=TEXT_ALIGN_LEFT, blank=True, null=True, choices=TEXT_ALIGN_CHOICES)
    vertical_align = models.CharField(max_length=255, default=VERTICAL_ALIGN_TOP, blank=True, null=True, choices=VERTICAL_ALIGN_CHOICES)

    font = models.ForeignKey('Font', blank=True, null=True)
    font_size = models.IntegerField(default='20', blank=True, null=True)
    font_color = models.CharField(max_length=255, blank=True, null=True, default="#000000")
    text_placeholder_type = models.CharField(max_length=100,choices=TEXT_PLACEHOLDER_TYPE_CHOICES, default=PERSON_NAME)

    action = models.CharField(max_length=255, null=True, blank=True, default='')

    content_type = models.SmallIntegerField(choices=CONTENT_TYPE_CHOICES, default=TEXT)
    label = models.CharField(max_length=255, default='')
    property = models.CharField(max_length=255, default='')


    def __unicode__(self):
        return '%i. %s' % (self.id, self.layout_element.label)

    # @property
    def file_name(self):
        return self.file.split('/')[-1]


    def action_name(self):
        action_name_str = ''

        if self.action:
            action_name_str = self.action.split('/')[-1]

        return action_name_str

    #
    # order_number = models.SmallIntegerField(default=1)
    #

    #
    # content_type = models.SmallIntegerField(choices=CONTENT_TYPE_CHOICES, default=TEXT)
    #



    #
    # predefined_text_content = models.TextField(default='', blank=True, null=True)
    # predefined_image = models.FileField(upload_to=set_file_name, max_length=255, null=True, blank=True, default='')
    #


class Language(models.Model):

    title = models.CharField(max_length=200)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.title)


class Asset(models.Model):

    subtype = models.CharField(max_length=255, null=True, blank=True, default='')

    brand = models.ForeignKey('Brand', related_name='brand_assets', null=True, blank=True)
    range = models.ForeignKey('Range', related_name='range_assets', null=True, blank=True)
    sku = models.ForeignKey('SKU', related_name='sku_assets', null=True, blank=True)

    file = models.FileField(upload_to=set_file_name, max_length=255)
    file_thumbnail = models.FileField(upload_to=set_file_name, max_length=255, null=True, blank=True, default='')
    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.file.name)

    @property
    def date_created_str(self):
        return self.date_created.strftime(settings.WEB_DATE_FORMAT)

    @property
    def short_name(self):
        return self.file.name.split('/')[-1][-10:]

    @property
    def file_name(self):
        return self.file.name.split('/')[-1]

    @property
    def parent_name(self):
        parent_name = ''

        if self.brand:
            parent_name = self.brand.title

        elif self.range:
            parent_name = self.range.title

        elif self.sku:
            parent_name = self.sku.title

        return parent_name

    @property
    def location(self):
        location_str = '/assets/'

        if self.brand:
            location_str = '/assets/brands/%s/%s/' % (self.brand.safe_title, self.subtype, )
        elif self.range:
            location_str = "/assets/brands/%s/ranges/%s/%s/" % (self.range.brand.safe_title, self.range.safe_title, self.subtype, )
        elif self.sku:
            location_str = "/assets/brands/%s/ranges/%s/skus/%s/%s/" % (
                self.sku.range.brand.safe_title,
                self.sku.range.safe_title,
                self.sku.safe_title,
                self.subtype,
            )

        return location_str

    @property
    def thumbnail_url(self):

        thumbnail_url = ''
        if self.file_thumbnail:
            thumbnail_url = self.file_thumbnail.url
        elif self.file:
            thumbnail_url = self.file.url

        return thumbnail_url

    def save(self, *args, **kwargs):

        from mcwilliams_wine_project.apps.web.functions import replace_spaces

        new_name = '/assets/%s' % self.file.name

        if self.brand:
            new_name = '/assets/brands/%s/%s/%s' % (self.brand.safe_title, self.subtype, self.file.name, )
        elif self.range:
            new_name = "/assets/brands/%s/ranges/%s/%s/%s" % (self.range.brand.safe_title, self.range.safe_title, self.subtype, self.file.name, )
        elif self.sku:
            new_name = "/assets/brands/%s/ranges/%s/skus/%s/%s/%s" % (
                self.sku.range.brand.safe_title,
                self.sku.range.safe_title,
                self.sku.safe_title,
                self.subtype,
                self.file.name,
            )

        self.file.name = replace_spaces(new_name)

        super(Asset, self).save(*args, **kwargs)

        try:
            if self.file:
                file_ext = self.file.name.split('.')[-1].lower()

                # is img
                if file_ext in settings.IMG_EXTS:
                    Asset.objects.filter(id=self.id).update(file_thumbnail=get_thumbnailer(self.file)['50'])

                # is video
                elif file_ext in settings.VIDEO_EXTS:
                    Asset.objects.filter(id=self.id).update(
                        file_thumbnail='/static/images/placeholders/asset/placeholder.png'
                    )
                # is pdf
                elif file_ext in ['pdf']:
                    Asset.objects.filter(id=self.id).update(
                        file_thumbnail='/static/images/placeholders/asset/pdf_placeholder.png'
                    )
                else:
                    Asset.objects.filter(id=self.id).update(
                        file_thumbnail='/static/images/placeholders/layout/any_file_placeholder.png'
                    )
        except:
            pass

    def delete(self, using=None):
        #
        # file_ext = self.file.name.split('.')[-1].lower()
        #
        # if self.file:
        #     self.file.delete(save=False)
        #
        # # Make sure we are not deleting video placeholder icon from static
        # if self.file_thumbnail and file_ext in settings.IMG_EXTS:
        #     self.file_thumbnail.delete(save=False)

        super(Asset, self).delete()

    #
    # class Meta:
    #     abstract = True


class AppVersion(models.Model):

    IOS_APP = 1

    APP_TYPE_CHOICES = (
        (IOS_APP, 'iOS'),
    )

    type = models.SmallIntegerField(choices=APP_TYPE_CHOICES, default=IOS_APP)
    value = models.CharField(max_length=255)

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s - %s' % (self.value, self.get_type_display())


class AppLogging(models.Model):

    IOS_APP = 1

    APP_TYPE_CHOICES = (
        (IOS_APP, 'iOS'),
    )

    type = models.SmallIntegerField(choices=APP_TYPE_CHOICES, default=IOS_APP)
    text = models.TextField()

    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s - %s' % (self.text, self.get_type_display())


class TempPreviewImage(models.Model):

    # user = models.ForeignKey('User')
    path = models.TextField(blank=True, null=True, default='')
    date_created = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    # def save(self, *args, **kwargs):
    #
    #     try:
    #         this = TempPreviewImage.objects.get(id=self.id)
    #         if this.file != self.file:
    #             this.file.delete(save=False)
    #     except: pass
    #
    #     super(TempPreviewImage, self).save(*args, **kwargs)


    def __unicode__(self):
        return '%s - %s' % (self.id, self.file.name)


class Font(models.Model):

    label = models.CharField(max_length=255)
    css_name= models.CharField(max_length=255)

    def __unicode__(self):
        return '%s. %s' % (self.label, self.css_name)


class Pricelist(models.Model):

    distributor = models.ForeignKey('Distributor', related_name='distributor_pricelists')
    state = models.ForeignKey('State', related_name='state_pricelists', blank=True, null=True, default=None)
    country = models.ForeignKey('Country', related_name='country_pricelists', blank=True, null=True, default=None)
    file = models.FileField(upload_to=set_file_name, max_length=255, default=None)

    def __unicode__(self):
        return '%i. %s' % (self.id, self.distributor.title)
