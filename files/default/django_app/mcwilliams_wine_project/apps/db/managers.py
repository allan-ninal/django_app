from django.contrib.auth.models import BaseUserManager
from django.utils import timezone


class UserManager(BaseUserManager):

    def _create_user(self, first_name, email, password, role, **extra_fields):
        """
        Creates and saves a User with the given name, email and password.
        """
        now = timezone.now()
        if not first_name:
            raise ValueError('The given name must be set')
        email = self.normalize_email(email)
        user = self.model(first_name=first_name, email=email, is_active=True,
                          role=role, last_login=now,
                          date_registered=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, email, password, role, **extra_fields):
        return self._create_user(name, email, password, role, **extra_fields)

    def create_superuser(self, first_name, email, password, **extra_fields):
        return self._create_user(first_name, email, password, 1, **extra_fields)