# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0008_auto_20150526_1513'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='date_of_birth',
        ),
        migrations.AddField(
            model_name='presentation',
            name='country',
            field=models.ForeignKey(related_name='country_presentations', blank=True, to='db.Country', null=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(default=b'', max_length=100, null=True, blank=True),
        ),
    ]
