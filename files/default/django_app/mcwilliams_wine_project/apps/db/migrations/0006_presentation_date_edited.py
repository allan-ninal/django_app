# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0005_pricelist'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='date_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 19, 14, 49, 51, 243717, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
