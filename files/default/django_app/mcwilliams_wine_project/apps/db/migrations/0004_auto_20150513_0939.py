# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mcwilliams_wine_project.apps.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0003_auto_20150513_0934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentation',
            name='archive',
            field=models.FileField(default=b'', max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True),
        ),
        migrations.AlterField(
            model_name='presentation',
            name='thumbnail',
            field=models.FileField(default=b'', max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True),
        ),
    ]
