# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='state',
            field=models.ForeignKey(related_name='state_presentations', blank=True, to='db.State', null=True),
        ),
    ]
