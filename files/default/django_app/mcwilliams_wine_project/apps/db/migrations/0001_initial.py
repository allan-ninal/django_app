# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings
import mcwilliams_wine_project.apps.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=200)),
                ('surname', models.CharField(max_length=100)),
                ('first_name', models.CharField(max_length=100)),
                ('phone', models.CharField(max_length=100)),
                ('date_of_birth', models.DateTimeField(null=True, blank=True)),
                ('details', models.TextField(default=b'', null=True, blank=True)),
                ('role', models.SmallIntegerField(default=2, choices=[(1, b'Master Admin'), (2, b'Content Manager'), (3, b'End User')])),
                ('is_active', models.BooleanField(default=True)),
                ('date_registered', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
        ),
        migrations.CreateModel(
            name='AppLogging',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.SmallIntegerField(default=1, choices=[(1, b'iOS')])),
                ('text', models.TextField()),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='AppVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.SmallIntegerField(default=1, choices=[(1, b'iOS')])),
                ('value', models.CharField(max_length=255)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Asset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subtype', models.CharField(default=b'', max_length=255, null=True, blank=True)),
                ('file', models.FileField(max_length=255, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name)),
                ('file_thumbnail', models.FileField(default=b'', max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('features', models.TextField(default=b'', null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Distributor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('details', models.TextField(default=b'', null=True, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('brands', models.ManyToManyField(related_name='brand_distributors', null=True, to='db.Brand', blank=True)),
                ('country', models.ForeignKey(related_name='country_distributors', blank=True, to='db.Country', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ElementType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255)),
                ('content_type', models.SmallIntegerField(default=1, choices=[(1, b'Text'), (2, b'Image'), (3, b'Video'), (4, b'Text placeholder')])),
                ('z_index', models.IntegerField(default=1)),
                ('text_placeholder_type', models.SmallIntegerField(default=0, choices=[(0, b'None'), (1, b'Person Name'), (2, b'Company Name'), (3, b'Date')])),
            ],
        ),
        migrations.CreateModel(
            name='Font',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=255)),
                ('css_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Layout',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('thumbnail', models.ImageField(default=b'', max_length=255, null=True, upload_to=b'layouts/', blank=True)),
                ('brand', models.ForeignKey(related_name='brand_layouts', blank=True, to='db.Brand', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='LayoutElement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_number', models.SmallIntegerField(default=1)),
                ('label', models.CharField(default=b'', max_length=255)),
                ('property', models.CharField(default=b'', max_length=255)),
                ('content_type', models.SmallIntegerField(default=1, choices=[(1, b'Text'), (2, b'Image'), (3, b'Video'), (4, b'Button'), (5, b'Text placeholder')])),
                ('x', models.IntegerField(default=0)),
                ('y', models.IntegerField(default=0)),
                ('width', models.IntegerField(default=0)),
                ('height', models.IntegerField(default=0)),
                ('z_index', models.IntegerField(default=1)),
                ('resizable', models.SmallIntegerField(default=1, choices=[(1, b'Yes'), (0, b'No')])),
                ('movable', models.SmallIntegerField(default=1, choices=[(1, b'Yes'), (0, b'No')])),
                ('line_height', models.CharField(default=b'100%', max_length=255, null=True, blank=True)),
                ('text_align', models.CharField(default=b'left', max_length=255, null=True, blank=True, choices=[(b'left', b'left'), (b'center', b'center'), (b'right', b'right')])),
                ('vertical_align', models.CharField(default=b'top', max_length=255, null=True, blank=True, choices=[(b'top', b'top'), (b'middle', b'middle'), (b'bottom', b'bottom')])),
                ('font_size', models.IntegerField(default=b'20', null=True, blank=True)),
                ('font_color', models.CharField(default=b'#000000', max_length=255, null=True, blank=True)),
                ('predefined_text_content', models.TextField(default=b'', null=True, blank=True)),
                ('predefined_image', models.FileField(default=b'', max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('text_placeholder_type', models.CharField(default=b'person_name', max_length=100, choices=[(b'none', b'None'), (b'person_name', b'Person Name'), (b'company_name', b'Company Name'), (b'date', b'Date')])),
                ('font', models.ForeignKey(blank=True, to='db.Font', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Presentation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField(default=b'', null=True, blank=True)),
                ('archive', models.FileField(default=b'', null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('thumbnail', models.FileField(default=b'', null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Range',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('brand', models.ForeignKey(related_name='brand_ranges', to='db.Brand')),
            ],
        ),
        migrations.CreateModel(
            name='SKU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('sku_num', models.CharField(max_length=10, null=True, blank=True)),
                ('vintage_num', models.CharField(max_length=4, null=True, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('range', models.ForeignKey(related_name='range_sku', to='db.Range')),
            ],
        ),
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('file', models.FileField(default=b'', upload_to=mcwilliams_wine_project.apps.db.models.set_file_name)),
                ('file_thumbnail', models.FileField(default=b'', upload_to=mcwilliams_wine_project.apps.db.models.set_file_name)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('brand', models.ForeignKey(related_name='brand_slides', blank=True, to='db.Brand', null=True)),
                ('language', models.ForeignKey(related_name='language_slides', blank=True, to='db.Language', null=True)),
                ('layout', models.ForeignKey(related_name='layout_slides', blank=True, to='db.Layout', null=True)),
                ('presentation', models.ForeignKey(related_name='presentation_slides', blank=True, to='db.Presentation', null=True)),
                ('range', models.ForeignKey(related_name='range_slides', blank=True, to='db.Range', null=True)),
                ('sku', models.ForeignKey(related_name='sku_slides', blank=True, to='db.SKU', null=True)),
                ('user', models.ForeignKey(related_name='user_slides', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SlideAsset',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FilePathField(path=b'/home/ubuntu/mcwilliams-web/mcwilliams_wine_project/media/assets', max_length=255, recursive=True)),
                ('content', models.TextField(default=b'', null=True, blank=True)),
                ('x', models.IntegerField(default=0)),
                ('y', models.IntegerField(default=0)),
                ('width', models.IntegerField(default=0)),
                ('height', models.IntegerField(default=0)),
                ('z_index', models.IntegerField(default=-1)),
                ('asset', models.ForeignKey(to='db.Asset')),
                ('slide', models.ForeignKey(to='db.Slide')),
                ('type', models.ForeignKey(blank=True, to='db.ElementType', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SlideElement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.CharField(max_length=255, null=True, blank=True)),
                ('content', models.TextField(default=b'', null=True, blank=True)),
                ('x', models.IntegerField(default=0)),
                ('y', models.IntegerField(default=0)),
                ('width', models.IntegerField(default=0)),
                ('height', models.IntegerField(default=0)),
                ('z_index', models.IntegerField(default=1)),
                ('resizable', models.SmallIntegerField(default=1, choices=[(1, b'Yes'), (0, b'No')])),
                ('movable', models.SmallIntegerField(default=1, choices=[(1, b'Yes'), (0, b'No')])),
                ('line_height', models.CharField(default=b'100%', max_length=255, null=True, blank=True)),
                ('text_align', models.CharField(default=b'left', max_length=255, null=True, blank=True, choices=[(b'left', b'left'), (b'center', b'center'), (b'right', b'right')])),
                ('vertical_align', models.CharField(default=b'top', max_length=255, null=True, blank=True, choices=[(b'top', b'top'), (b'middle', b'middle'), (b'bottom', b'bottom')])),
                ('font_size', models.IntegerField(default=b'20', null=True, blank=True)),
                ('font_color', models.CharField(default=b'#000000', max_length=255, null=True, blank=True)),
                ('text_placeholder_type', models.CharField(default=b'person_name', max_length=100, choices=[(b'none', b'None'), (b'person_name', b'Person Name'), (b'company_name', b'Company Name'), (b'date', b'Date')])),
                ('action', models.CharField(default=b'', max_length=255, null=True, blank=True)),
                ('content_type', models.SmallIntegerField(default=1, choices=[(1, b'Text'), (2, b'Image'), (3, b'Video'), (4, b'Button'), (5, b'Text placeholder')])),
                ('label', models.CharField(default=b'', max_length=255)),
                ('property', models.CharField(default=b'', max_length=255)),
                ('font', models.ForeignKey(blank=True, to='db.Font', null=True)),
                ('layout_element', models.ForeignKey(to='db.LayoutElement')),
                ('slide', models.ForeignKey(to='db.Slide')),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('price_list', models.FileField(max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('country', models.ForeignKey(related_name='country_states', to='db.Country')),
            ],
        ),
        migrations.CreateModel(
            name='TempPreviewImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('path', models.TextField(default=b'', null=True, blank=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='presentation',
            name='sku',
            field=models.ForeignKey(related_name='sku_presentations', blank=True, to='db.SKU', null=True),
        ),
        migrations.AddField(
            model_name='presentation',
            name='slides',
            field=models.ManyToManyField(related_name='slide_presentations', to='db.Slide'),
        ),
        migrations.AddField(
            model_name='presentation',
            name='user',
            field=models.ForeignKey(related_name='user_presentations', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='layout',
            name='elements',
            field=models.ManyToManyField(related_name='layouts', to='db.LayoutElement'),
        ),
        migrations.AddField(
            model_name='brand',
            name='countries',
            field=models.ManyToManyField(related_name='country_brands', null=True, to='db.Country', blank=True),
        ),
        migrations.AddField(
            model_name='asset',
            name='brand',
            field=models.ForeignKey(related_name='brand_assets', blank=True, to='db.Brand', null=True),
        ),
        migrations.AddField(
            model_name='asset',
            name='range',
            field=models.ForeignKey(related_name='range_assets', blank=True, to='db.Range', null=True),
        ),
        migrations.AddField(
            model_name='asset',
            name='sku',
            field=models.ForeignKey(related_name='sku_assets', blank=True, to='db.SKU', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='distributor',
            field=models.ForeignKey(related_name='distributor_users', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='db.Distributor', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
