# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0009_auto_20150527_1430'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentation',
            name='countries',
            field=models.ManyToManyField(related_name='country_presentations_m2m', null=True, to='db.Country', blank=True),
        ),
    ]
