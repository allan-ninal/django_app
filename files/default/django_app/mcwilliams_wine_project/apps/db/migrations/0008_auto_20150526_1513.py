# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0007_auto_20150525_1411'),
    ]

    operations = [
        migrations.AddField(
            model_name='pricelist',
            name='country',
            field=models.ForeignKey(related_name='country_pricelists', default=None, blank=True, to='db.Country', null=True),
        ),
        migrations.AlterField(
            model_name='pricelist',
            name='state',
            field=models.ForeignKey(related_name='state_pricelists', default=None, blank=True, to='db.State', null=True),
        ),
    ]
