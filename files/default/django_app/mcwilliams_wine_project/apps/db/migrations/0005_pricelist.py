# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mcwilliams_wine_project.apps.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0004_auto_20150513_0939'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pricelist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(max_length=255, null=True, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name, blank=True)),
                ('distributor', models.ForeignKey(related_name='distributor_pricelists', to='db.Distributor')),
            ],
        ),
    ]
