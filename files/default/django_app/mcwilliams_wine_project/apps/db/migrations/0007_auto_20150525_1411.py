# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import mcwilliams_wine_project.apps.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0006_presentation_date_edited'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributor',
            name='states',
            field=models.ManyToManyField(related_name='state_distributors', null=True, to='db.State', blank=True),
        ),
        migrations.AddField(
            model_name='presentation',
            name='distributors',
            field=models.ManyToManyField(related_name='distributor_presentations', null=True, to='db.Distributor', blank=True),
        ),
        migrations.AddField(
            model_name='pricelist',
            name='state',
            field=models.ForeignKey(related_name='state_pricelists', default=None, to='db.State'),
        ),
        migrations.AddField(
            model_name='user',
            name='country',
            field=models.ForeignKey(related_name='country_users', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='db.Country', null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='state',
            field=models.ForeignKey(related_name='state_users', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='db.State', null=True),
        ),
        migrations.AlterField(
            model_name='pricelist',
            name='file',
            field=models.FileField(default=None, max_length=255, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name),
        ),
    ]
