# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mcwilliams_wine_project.apps.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0002_presentation_state'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='file',
            field=models.FileField(default=b'', max_length=255, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name),
        ),
        migrations.AlterField(
            model_name='slide',
            name='file_thumbnail',
            field=models.FileField(default=b'', max_length=255, upload_to=mcwilliams_wine_project.apps.db.models.set_file_name),
        ),
    ]
