"""
Django settings for mcwilliams_wine_project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&af*2l+vf81mrj)p)5g-76q2blah+bwm6aofnbj!0l&w4egp4j'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

USE_S3 = True

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mcwilliams_wine_project.apps.api',
    'mcwilliams_wine_project.apps.web',
    'mcwilliams_wine_project.apps.db',
    'mcwilliams_wine_project.apps.general_functions',
    'mcwilliams_wine_project.apps.error_monitor',
    'jfu',
    'easy_thumbnails',
    'reportlab'
)

if USE_S3:
    INSTALLED_APPS += ('storages',)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'mcwilliams_wine_project.apps.error_monitor.middleware.ExceptionMiddleware'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.static'
)

ROOT_URLCONF = 'mcwilliams_wine_project.urls'

WSGI_APPLICATION = 'mcwilliams_wine_project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mcwilliams_wine_db',
        'USER': 'mcwilliamsmaster',
        'PASSWORD': '777mcwilliams_db_master777',
        'HOST': 'django-opsworks.cmfyw35ux9ic.ap-southeast-2.rds.amazonaws.com',
        'PORT': '5432',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

# STATIC_ROOT = os.path.join(BASE_DIR, 'mcwilliams_wine_project', "static")

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'mcwilliams_wine_project', "static"),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)


# Media

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'mcwilliams_wine_project', 'media')


# Templates

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'mcwilliams_wine_project', 'templates'),
)


## Custom User Model

AUTH_USER_MODEL = 'db.User'

## Auth Backends

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)


## Login settings

LOGIN_URL = "/login/"

LOGIN_REDIRECT_URL = "/"


# API Date format

API_DATE_FORMAT = "%d/%m/%Y"


# API DateTime format

API_DATETIME_FORMAT = "%d/%m/%YT%H:%M:%S"


# Web Date format

WEB_DATE_FORMAT = '%b %d, %Y'


# EMAIL Configs

EMAIL_HOST = 'smtp.gmail.com'

EMAIL_PORT = 587

EMAIL_HOST_USER = 'stivickusus@gmail.com'

EMAIL_HOST_PASSWORD = '19216855110akka'

EMAIL_USE_TLS = True


# Easy Thumbnails

THUMBNAIL_ALIASES = {
    '': {
        '50': {
            'size': (50, 50),
            'crop': "smart",
            'quality': 100
        },
        'slide_thumbnail': {
            'size': (107, 80),
            'crop': "smart",
            'quality': 100
        },
        '200': {'size': (200, 200), 'crop': "smart", 'quality': 100},
        'slide_background': {'size': (1024, 768), 'crop': "smart", 'quality': 100, 'upscale': True}
        # '110x72' : {'size': (110, 72), 'crop': False , 'quality' : 100},
        }
    }

THUMBNAIL_PREFIX = 'thumbnails/'

THUMBNAIL_NAMER = 'easy_thumbnails.namers.hashed'

THUMBNAIL_DEBUG = True


# Assets options

ASSETS_ROOT = os.path.join(MEDIA_ROOT, 'assets')
ASSETS_URL = os.path.join(MEDIA_URL, 'assets/')

IMG_EXTS = ['jpeg', 'jpg', 'png']
VIDEO_EXTS = ['webm', 'mkv', 'flv', 'vob', 'ogg', 'ogv', 'avi', 'wmv', 'mp4', 'm4p', 'mpg', 'm4v', '']


# S3 Static Handling Options

if USE_S3:

    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    # DEFAULT_FILE_STORAGE = 'mcwilliams_wine_project.apps.web.functions.MediaFilesStorage'
    THUMBNAIL_DEFAULT_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

    FILE_REMOTE_STORAGE = True
    THUMBNAIL_REMOTE_STORAGE = True
    EASY_CACHE_TIMEOUT = 60 * 60 * 24 * 30

    AWS_ACCESS_KEY_ID = 'AKIAISLXONYGPIAZBJSA'
    AWS_SECRET_ACCESS_KEY = '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'
    AWS_STORAGE_BUCKET_NAME = 'mcwilliams'

    AWS_QUERYSTRING_AUTH = False
    AWS_S3_HOST = "ap-southeast-2"
    AWS_S3_CUSTOM_DOMAIN = '%s.s3-%s.amazonaws.com' % (AWS_STORAGE_BUCKET_NAME, AWS_S3_HOST, )



    # MEDIA_URL = "https://dnn13921hrox3.cloudfront.net/"

CURRENT_DOMAIN = 'http://52.64.102.141'
