#!/bin/bash

PROJECT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

kill -9 `cat $PROJECT_PATH/mcwilliams_wine_project/django.pid`
$PROJECT_PATH/manage.py runfcgi --settings=mcwilliams_wine_project.settings minspare=2 maxspare=7 requests=150 method=prefork socket=$PROJECT_PATH/mcwilliams_wine_project/django.sock pidfile=$PROJECT_PATH/mcwilliams_wine_project/django.pid daemonize=true
chmod 777 $PROJECT_PATH/mcwilliams_wine_project/django.sock
